<?php
  session_start();
  $user = $_SESSION['username'];
  //$api_url=$_SESSION['url'];
  if( (!isset($_SESSION['valid'])) || ($_SESSION['valid']!=1)){
    header('Location: ../index.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>AGC-RFID</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../CSS/bootstrap.min.css">
  <link rel="stylesheet" href="../CSS/fonts.css">
  <link rel="stylesheet" href="../CSS/fontawesome-all.css">
  <link rel="stylesheet" href="../CSS/fontawesome-all.min.css">
  <link rel="stylesheet" href="../CSS/sidenav.css">
  <link rel="stylesheet" href="../CSS/style.css">
  <link rel="stylesheet" href="../CSS/datatables.css">
  <link rel="stylesheet" type="text/css" href="../CSS/jquery.datetimepicker.css"/>
  <style type="text/css">


  </style>
</head>
<body>
  <!-- As a heading -->
  <div id="nav"></div>

<br><br><br><br>
<div class="container-fluid">
  <div class="row">
    <div class="col-lg-2 col-md-2"></div>
    <div class="col-lg-10 col-md-10">
    <div class="panel panel-body card" id="main_panel">
      <div class="container-fluid ">
        <br>
        <div class="row">
            <div class="col-lg-4 col-md-4">
              <br>
              <h1 style="margin-left:30px"><i class="fas fa-diagnoses"></i> สถานะการมาทำงาน</h1>
            </div>

              <div class="col-lg-5 col-md-5">
                <div class="vl"></div>
                <div class="row">
                  <div class="col-lg-2 col-md-2"></div>
                  <div class="col-lg-5 col-md-5">
                    <div class="form-group">
                      <label for="exampleFormControlFile1">วันที่ค้นหา</label>
                      <input type="text" class="form-control" placeholder="วันที่ค้นหา" id="start_date" value="">
                    </div>
                  </form>
                  </div>
                  <!-- <div class="col-lg-5 col-md-5">
                    <div class="form-group">
                      <label for="exampleFormControlFile1">วันที่สิ้นสุดค้นหา</label>
                      <input type="text" class="form-control" placeholder="วันที่ค้นหา" id="end_date" value="">
                    </div>
                  </form>
                  </div> -->
                  <div class="col-lg-2 col-md-2">
                    <br>
                    <button id="search" name="search" type="submit" class="btn btn-primary btn-lg">ดูข้อมูล</button>
                  </div>

                </div>
              </div>

              <div class="col-lg-3 col-md-3">
                <div class="vl"></div>
                <div class="row">
                  <div class="col-lg-3 col-md-3"></div>
                  <div class="col-lg-6 col-md-6">
                      <h3 id="real_date"></h3>
                    <hr>
                      <h3 id='txt' ><h3>
                  </div>
                  <div class="col-lg-3 col-md-3"></div>
                </div>
              </div>

        </div>
        <hr>
        <div class="container-fluid" id="head_t">
          <table class="table" id="main_table">
            <thead>
              <tr class="tr_head tr_color">
                <th scope="col">รหัสพนักงาน</th>
                <th scope="col">ชื่อต้น</th>
                <th scope="col">ชื่อ</th>
                <th scope="col">นามสกุล</th>
                <th scope="col">เบอร์โทร</th>
                <th scope="col">ตำแหน่ง</th>
                <th scope="col">สถานะ</th>
              </tr>
            </thead>
            <tbody id="body_t">
              <!-- insert_data -->
            </tbody>
          </table>
        </div>
        <br>
        <div id="spinner">
            <center><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
            <span class="sr-only">Loading...</span></center>
        </div>
        <br>
      </div>
    </div>
  </div>
</div>
</div>
<!-- ======== UserName Modal ========-->
<div id="modal"></div>

<!-- JS -->
<script src="../JS/config.js"></script>
<script src="../JS/nav.js"></script>
<script src="../JS/jquery.min.js"></script>
<script src="../JS/bootstrap.min.js"></script>
<script src="../JS/jquery.datetimepicker.full.min.js"></script>
<script src="../JS/datatables.js"></script>
<script src="../JS/moment.js"></script>
<script src="../JS/moment.min.js"></script>

<script src="../JS/underscore-min.js"></script>
<script src="../JS/pdfmake.min.js"></script>
<script src="../JS/vfs_fonts.js"></script>
<script src="../JS/underscore-min.js"></script>


<script type="text/javascript">
  var data,st_date,end_date;
  var g_table,check_status;

  $(document).ready(function() {
    // nav
    make_nav();
    admin("<?php echo $_SESSION['username'];?>");
    user();
    // nav
    $('#real_date').text(moment().format('DD/MM/YYYY'));
    $('#txt').text(moment().format('HH : mm : ss'));
    time();
    $('#start_date').datetimepicker({
       timepicker:false,
       format:'Y-m-d'
    });
    $('#end_date').datetimepicker({
       timepicker:false,
       format:'Y-m-d'
    });

    $('#start_date').val(moment().format('YYYY-MM-DD'));
    // $('#end_date').val(moment().format('YYYY-MM-DD'));
    $('#start_date').val('2018-10-10');
    // $('#end_date').val('2017-11-30');
    try {
      search();
      $('#search').click();
    }
    catch(e) {
      // console.log(e);
    }

  });

  function make_nav(){

    nav("staff");

    $('#dd').append(
      '<td> '+"<?php echo $_SESSION['username'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['name'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['surname'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['email'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['phone'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['create_date'];?>"+' </td>'
    );

  }

  function search(){
    $('#search').click(function(){
      $('#head_t').empty();
      $('#head_t').append(
        '<table class="table" id="main_table">'+
          '<thead>'+
            '<tr class="tr_head tr_color">'+
              '<th scope="col">รหัสพนักงาน</th>'+
              '<th scope="col">ชื่อต้น</th>'+
              '<th scope="col">ชื่อ</th>'+
              '<th scope="col">นามสกุล</th>'+
              '<th scope="col">แผนก</th>'+
              '<th scope="col">เบอร์โทร</th>'+
              '<th scope="col">ตำแหน่ง</th>'+
              '<th scope="col">เวลาเข้า</th>'+
              '<th scope="col">เวลาออก</th>'+
              '<th scope="col">สถานะ</th>'+
            '</tr>'+
          '</thead>'+
          '<tbody id="body_t">'+
            '<!-- insert_data -->'+
          '</tbody>'+
        '</table>'

      );


      $('#body_t').empty();

      //---------------------------------------------------------------------------
      st_date = $('#start_date').val()+" 00:00:00";
      end_date = $('#start_date').val()+" 23:59:59";
      $('#spinner').show();
      data_table();
    });
  }

  function data_table(){

    var settings = {
      "async": true,
      "crossDomain": true,

      "url": "../pages/api/api_staff.php?start="+st_date+"&end="+end_date,
      "method": "GET"
    }

    $.ajax(settings).done(function (response) {
      // try {
        data = JSON.parse(response)
        insert_data(data);
      // } catch (e) {
      //   console.log(e);
      //   alert('ไม่พบข้อมูล');
      //   $('#spinner').hide();
      // }
    });
  }

  function insert_data(data_j){
    console.log(data_j);

    var time_in="-",time_out="-",index=0;

    var g_emp = _.groupBy(data_j.List,'empn');

    _.each(g_emp,function(val){
      // console.log(val);
      var temp_3 = _.groupBy(val,'ip');

      var data_in = [];
      for(var i=0; i<d_in.length; i++){
        data_in = _.union(data_in,temp_3[d_in[i]])
      }

      var data_out = [];
      for(var i=0; i<d_in.length; i++){
        data_out = _.union(data_out,temp_3[d_out[i]])
      }

      var data_in_sort = _.sortBy(data_in,"date");
      var data_out_sort = _.sortBy(data_out,"date");

      time_in = _.first(data_in_sort);
      time_out = _.last(data_out_sort);

      if(time_in){
        var time_o = "-";
        if(time_out){
          time_o = time_out.date;
        }
        $('#body_t').append(
            '<tr class="tr_body">'+
              '<th scope="row">'+time_in.empn+'</th>'+
              '<td >'+time_in.title+'</td>'+
              '<td>'+time_in.fname+'</td>'+
              '<td>'+time_in.surname+'</td>'+
              '<td>'+time_in.section2+'</td>'+
              '<td>'+time_in.tel+'</td>'+
              '<td>'+time_in.type+'</td>'+
              '<td>'+time_in.date+'</td>'+
              '<td>'+time_o+'</td>'+
              // '<td">-</td>'+
              '<td width="15%" align="center" bgcolor="'+status_color(time_in,time_o)+'"><h5 style="color:#F4F6F7">'+status()+'</h5></td>'+//มาต่อ
            '</tr>'
          );

      }else if (time_out) {
        var time_i = "-";
        if(time_in){
          time_i = time_in.date;
        }
        $('#body_t').append(
            '<tr class="tr_body">'+
              '<th scope="row">'+time_out.empn+'</th>'+
              '<td >'+time_out.title+'</td>'+
              '<td>'+time_out.fname+'</td>'+
              '<td>'+time_out.surname+'</td>'+
              '<td>'+time_out.section2+'</td>'+
              '<td>'+time_out.tel+'</td>'+
              '<td>'+time_out.type+'</td>'+
              '<td>-</td>'+
              '<td>'+time_out.date+'</td>'+
              '<td width="15%" align="center" bgcolor="'+status_color(time_i,time_out)+'"><h5 style="color:#F4F6F7">'+status()+'</h5></td>'+
            '</tr>'
          );
      }
      index++;

    });


    // for(var i=0; i<data_j.List0.length; i++){
    //   $('#body_t').append(
    //       '<tr class="tr_body">'+
    //       '<th scope="row">'+data_j.List0[i].empn+'</th>'+
    //       '<td >'+data_j.List0[i].title+'</td>'+
    //       '<td>'+data_j.List0[i].fname+'</td>'+
    //       '<td>'+data_j.List0[i].surname+'</td>'+
    //       '<td>'+data_j.List0[i].section2+'</td>'+
    //       '<td>'+data_j.List0[i].tel+'</td>'+
    //       '<td>'+data_j.List0[i].type+'</td>'+
    //       '<td>'+time_in+'</td>'+
    //       '<td>'+time_out+'</td>'+
    //       '<td width="15%" align="center" bgcolor="'+status_color(data_j.List0[i].empn,data_j)+'"><h5 style="color:#F4F6F7">'+status()+'</h5></td>'+
    //     '</tr>'
    //   );
    // }

    $('#main_table').DataTable({
        dom: 'Bfrtip',
         buttons: [
            {
                extend: 'excelHtml5',
                title: 'Exel_staff'
            }
        ]
    });
    $('#spinner').hide();
  }

  function status_color(time_in_c,time_out_c){
    // check_status = 0;
    var time_in = "-";
    var time_out = "-";
    var time_in = time_in_c.date;
    var time_out = time_out_c;
    // console.log(time_in);
    // console.log(time_out);

    if(time_in!="-" && time_out=="-"){
      check_status = 1;
    }else if(time_in!="-" && time_out!="-"){
      check_status = 2;
    }else if(time_in=="-" && time_out!="-"){
      check_status = 2;
    }else if(time_in=="-" && time_out=="-"){
      check_status = 0;
    }

    console.log(check_status);

    if(check_status == 1)//มาทำงาน
    return "#52BE80";
    if(check_status == 2)//ออกกะแล้ว
    return "#008dff";
    if(check_status == 0)//ไม่มาทำงาน
    return "#DF0101";
    else
    return "#A4A4A4";
  }

  function status(){
    if(check_status == 1)
    return "มาทำงาน";
    if(check_status == 2)
    return "ออกกะแล้ว";
    if(check_status == 0)
    return "ไม่มาทำงาน";
    else
    return "Unknown";
  }


  // ----------------------------time-------------------
  function time(){
    setInterval(function(){
      $('#real_date').text(moment().format('DD/MM/YYYY'));
       $('#txt').text(moment().format('HH : mm : ss'));
    }, 1000);
  }



</script>

</body>
</html>
