<?php session_start();
  $user = $_SESSION['username'];
  //$api_url=$_SESSION['url'];
  if( (!isset($_SESSION['valid'])) || ($_SESSION['valid']!=1)){
    header('Location: ../index.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>AGC-RFID</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../CSS/bootstrap.min.css">
  <link rel="stylesheet" href="../CSS/fonts.css">
  <link rel="stylesheet" href="../CSS/fontawesome-all.css">
  <link rel="stylesheet" href="../CSS/fontawesome-all.min.css">
  <link rel="stylesheet" href="../CSS/sidenav.css">
  <link rel="stylesheet" href="../CSS/style.css">
  <link rel="stylesheet" href="../CSS/datatables.css">
  <link rel="stylesheet" type="text/css" href="../CSS/jquery.datetimepicker.css"/>
  <style type="text/css">

  </style>
</head>
<body>
  <!-- As a heading -->
  <div id="nav"></div>

<br><br><br><br>
<div class="container-fluid">
  <div class="row">
    <div class="col-lg-2 col-md-2"></div>
    <div class="col-lg-10 col-md-10">
    <div class="panel panel-body card" id="main_panel">
      <div class="container-fluid ">
        <br>
        <div class="row">
            <div class="col-lg-4 col-md-4">
              <br>
              <h1><i class="fas fa-align-left" style="margin-left:30px"></i> ตารางข้อมูลดิบ</h1>
              <!-- <div class="vll"></div> -->
            </div>

              <div class="col-lg-5 col-md-5">
                <div class="vl"></div>
                <div class="row">
                  <div class="col-lg-5 col-md-5">
                    <div class="form-group">
                      <label for="exampleFormControlFile1">วันที่เริ่มการค้นหา</label>
                      <input type="text" class="form-control" placeholder="วันที่ค้นหา" id="start_date" value="">
                    </div>
                  </form>
                  </div>
                  <div class="col-lg-5 col-md-5">
                    <div class="form-group">
                      <label for="exampleFormControlFile1">วันที่สิ้นสุดการค้นหา</label>
                      <input type="text" class="form-control" placeholder="วันที่ค้นหา" id="end_date" value="">
                    </div>
                  </form>
                  </div>
                  <div class="col-lg-2 col-md-2">
                    <br>
                    <button id="search" name="search" type="submit" class="btn btn-primary btn-lg">ดูข้อมูล</button>
                  </div>

                </div>
              </div>

              <div class="col-lg-3 col-md-3">
                <div class="vl"></div>
                <div class="row">
                  <div class="col-lg-3 col-md-3"></div>
                  <div class="col-lg-6 col-md-6">
                      <h3 id="real_date"></h3>
                    <hr>
                      <h3 id='txt' ><h3>
                  </div>
                  <div class="col-lg-3 col-md-3"></div>
                </div>
              </div>

        </div>
        <hr>
        <div class="container-fluid" id="head_t">
          <table class="table" id="main_table">
            <thead>
              <tr class="tr_head tr_color">
                <th scope="col">รหัสพนักงาน</th>
                <th scope="col">ชื่อต้น</th>
                <th scope="col">ชื่อ</th>
                <th scope="col">นามสกุล</th>
                <th scope="col">IP</th>
                <th scope="col">วิธีลงเวลา</th>
                <th scope="col">วันที่-เวลา</th>
              </tr>
            </thead>
            <tbody id="body_t">
              <!-- insert_data -->
            </tbody>
          </table>
        </div>
        <br>
        <div id="spinner">
            <center><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
            <span class="sr-only">Loading...</span></center>
        </div>
        <br>
      </div>
    </div>
  </div>
</div>
</div>
<!-- ======== UserName Modal ========-->
<!-- <div id="modal"></div> -->
<div class="modal fade bd-example-modal-lg" id="modal" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content" id="modal_content">
      <div class="modal-header"></div>
      <div class="modal-body"></div>
      <div class="modal-footer"></div>
    </div>
  </div>
</div>

<!-- JS -->
<script src="../JS/nav.js"></script>
<script src="../JS/jquery.min.js"></script>
<script src="../JS/bootstrap.min.js"></script>
<script src="../JS/jquery.datetimepicker.full.min.js"></script>
<script src="../JS/datatables.js"></script>
<script src="../JS/moment.min.js"></script>

<script src="../JS/underscore-min.js"></script>
<script src="../JS/pdfmake.min.js"></script>
<script src="../JS/vfs_fonts.js"></script>
<script src="../JS/download.js"></script>

<script type="text/javascript">

  var data,st_date,end_date;
  var g_table;

  var d_in = ['172.16.73.150','172.16.73.152','172.16.73.154'];
  var d_out = ['172.16.73.151','172.16.73.153','172.16.73.155','172.16.73.156'];

  $(document).ready(function() {
    // nav
    make_nav();
    admin("<?php echo $_SESSION['username'];?>");
    user();
    // nav
    $('#real_date').text(moment().format('DD/MM/YYYY'));
    $('#txt').text(moment().format('HH : mm : ss'));
    time();

    $('#start_date').datetimepicker({
       timepicker:false,
       format:'Y-m-d'
    });
    $('#end_date').datetimepicker({
       timepicker:false,
       format:'Y-m-d'
    });
    $('#start_date').val(moment().format('YYYY-MM-DD'));
    $('#end_date').val(moment().format('YYYY-MM-DD'));
    // $('#start_date').val('2018-10-10');
    // $('#end_date').val('2018-10-10');

    try {
      search();
      $('#search').click();
    }
    catch(e) {
      // console.log(e);
    }

  });

  function make_nav(){

    nav("raw");

    $('#dd').append(
      '<td> '+"<?php echo $_SESSION['username'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['name'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['surname'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['email'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['phone'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['create_date'];?>"+' </td>'
    );

  }

  function search(){
    $('#search').click(function(){
      $('#head_t').empty();
      $('#head_t').append(
        '<table class="table" id="main_table">'+
          '<thead>'+
            '<tr class="tr_head tr_color">'+
              '<th scope="col">รหัสพนักงาน</th>'+
              '<th scope="col">ชื่อต้น</th>'+
              '<th scope="col">ชื่อ</th>'+
              '<th scope="col">นามสกุล</th>'+
              '<th scope="col">IP</th>'+
              '<th scope="col">วิธีลงเวลา</th>'+
              '<th scope="col">วันที่-เวลา</th>'+
          '</thead>'+
          '<tbody id="body_t">'+
            '<!-- insert_data -->'+
          '</tbody>'+
        '</table>'

      );


      $('#body_t').empty();

      //---------------------------------------------------------------------------
      st_date = $('#start_date').val()+" 00:00:00";
      end_date = $('#end_date').val()+" 23:59:59";
      $('#spinner').show();
      data_table();
    });
  }

  function data_table(){

    var settings = {
      "async": true,
      "crossDomain": true,

      "url": "../pages/api/api_taa_emp_n.php?start="+st_date+"&end="+end_date,
      "method": "GET"
    }

    $.ajax(settings).done(function (response) {
      // data = JSON.parse(response)
      // insert_data_n(data);
      try {
        data = JSON.parse(response)
        insert_data_n(data);
      } catch (e) {
        // console.log(e);
        alert('ไม่พบข้อมูล');
        $('#spinner').hide();
      }
    });
  }

  function insert_data_n(data_j){
      var te = 0;
      console.log("!!! Raw !!!");
      console.log(data_j);
      
      _.each(data_j.List, function(val){
        console.log(val.card_log);
        $('#body_t').append(
            // '<tr class="tr_body">'+
            '<tr  class="tr_body" data-toggle="modal" data-target="#modal" value="'+val.card+','+filter_date(val.date_serv,0)+','+filter_date(val.date_serv,1)+'">'+
              '<th scope="row">'+ val.empn +'</th>'+
              '<td >'+val.title+'</td>'+
              '<td>'+val.fname+'</td>'+
              '<td>'+val.surname+'</td>'+
              '<td>'+check_ip(val.IP)+'</td>'+
              '<td>'+check_active(val.card_log)+'</td>'+
              '<td>'+val.date+'</td>'+
            '</tr>'
        );
      });  
      
    
      // --------------------------- Data Table -----------------------
      $('#main_table').DataTable({
            dom: 'Bfrtip',
            buttons: [
              {
                    text: 'TAF',
                    action: function ( e, dt, button, config ) {
                        var data = dt.buttons.exportData();
                        map_data(data);
                    }
                }
            ]
          });
      $('#spinner').hide();
      mainclick();
      // --------------------------- Data Table -----------------------
  }
  
  function calculate_time(){

    var table = $("#body_t");

    table.find('tr').each(function (i) {
       var $tds = $(this).find('td'),
           productId = $tds.eq(3).text(),
           a =  moment($tds.eq(4).text(),"HH:mm:ss"),
           b =  moment($tds.eq(5).text(),"HH:mm:ss"),
           sum = b.diff(a,"second"),
           formatted = moment.utc(sum*1000).format('HH:mm' + " ชม.");
           console.log(sum);
           $tds.eq(6).text(formatted);
   });

  }

  function map_data(data){

    var ms = data.body;
    console.log(ms);

    var auto_content='';
    var text;


      for(var j=0;j<ms.length;j++){
        if(ms[j][5] != "-"){
          if(ms[j][4] == 'เครื่องเข้า'){
            text = ms[j][0]+'   I '+taf_date(filter_date(ms[j][5],0))+' '+taf_time(filter_date(ms[j][5],1))+' 01';
            auto_content = auto_content+text+'\r\n';
          }
          if(ms[j][4] == 'เครื่องออก'){
            text = ms[j][0]+'   O '+taf_date(filter_date(ms[j][5],0))+' '+taf_time(filter_date(ms[j][5],1))+' 02';
            auto_content = auto_content+text+'\r\n';
          }
        }
      }

      pdfMake.fonts = {
        THSarabunNew: {
          normal: 'THSarabunNew.ttf',
          bold: 'THSarabunNew-Bold.ttf',
          italics: 'THSarabunNew-Italic.ttf',
          bolditalics: 'THSarabunNew-BoldItalic.ttf'
        },
        Roboto: {
          normal: 'Roboto-Regular.ttf',
          bold: 'Roboto-Medium.ttf',
          italics: 'Roboto-Italic.ttf',
          bolditalics: 'Roboto-MediumItalic.ttf'
        }
      }

      var docDefinition = {

        content : auto_content,

        defaultStyle:{
          font:'THSarabunNew'
        },

      };

      console.log(auto_content);

      download(auto_content, "Raw.TAF", "text/plain");

  }

  // ----------------------------date TAF---------------

  function filter_date(date,select){
    if(select == 0){
      var date = date.split(" ")[0];
      // console.log(date);
      return date;
    }
    else if(select == 1){
      var time = date.split(" ")[1];
      // console.log(time);
      return time;
    }
    else{
      return "-";
    }

  }

  function taf_date(date){
    var data = date.split('-');
    return ''+data[0]+data[1]+data[2]
  }

  function taf_time(date){
    var data = date.split(':');
    return ''+data[0]+data[1]
  }

  // ----------------------------Check IP----------------
  function check_ip(ip){
    for(var ip_position=0; ip_position<d_in.length; ip_position++){
      if(ip == d_in[ip_position]){
        return 'เครื่องเข้า';
        break;
      }
    }
    for(var ip_position1=0; ip_position1<d_out.length; ip_position1++){
      if(ip == d_out[ip_position1]){
        return 'เครื่องออก';
        break;
      }
    }
  }

  // ------------------------Check Active----------------
  function check_active(card){
    var res = card.substring(0, 2);
    if(res == "FN")
      return "ลายนิ้วมือ";
    else
      return "บัตรพนักงาน";
  }

  // --------------------------show img-----------------
  function mainclick(){
      var id,emp_num;
      $(".table").on('click','tr',function(e){
        console.log($(this).attr('value'));
        id = $(this).attr('value');

        var $ths = $(this).find('th');
        emp_num = $ths.eq(0).text();

        reset_modal();
        console.log(id + " " + emp_num);
        create_modal(id,emp_num);
      });

  }

  function reset_modal(){
        var modal = document.getElementById("modal_content");

        modal.innerHTML=
            '<div class="modal-header" id="modal_header">'+
            '</div>'+
            '<div class="row modal-body" id="modal_body">'+
            '</div>'+
            '<div class="modal-footer" id="modal_footer">'+
            '</div>';
  }

  function create_modal(id,emp_num){
      console.log(id);

      var dir = '../img/SNAP_IMG/';
      var emp_num_loc = "../img/ID_IMG/"+emp_num+".png";

      // var dir = '/home/timeatt/TAA/GO_SERVER/SNAP_IMG/';
      var img_in,img_out;

      var settings = {
        "async": true,
        "crossDomain": true,
        "url": "../pages/api/api_taa_img.php?data="+id,
        "method": "GET"
      }

      $.ajax(settings).done(function (response) {
        console.log(response);
        var response = JSON.parse(response);
        console.log(response);
        
        if(response.Total == 0){
          img_in = "../img/photo_ava.png";
        }else{
          img_in = dir+response.List[0];
        }
        $('#modal_header').append(
            '<h3 class="modal-title"><i class="far fa-images"></i> รูปภาพ</h3>'+
            '<button type="button" class="close" data-dismiss="modal">&times;</button>'
          );

          $('#modal_body').append(
            '<div class="container-fluid">'+
              '<div class="span12" >'+
                '<h2>รูปพนักงาน รหัส '+emp_num+'</h2>'+
                '<center><img src="'+emp_num_loc+'" alt="< Picture IN >" width="200" height="300"></center>'+
                '<hr>'+
              '</div>'+
              '<div class="span12" >'+
                '<h2>รูปภาพ</h2>'+
                '<center><img src="'+img_in+'" alt="< Picture IN >" width="460" height="345"></center>'+
              '</div>'+
            '</div>'
          );
        
      });
  }

  // ----------------------------time--------------------
  function time(){
    setInterval(function(){
      $('#real_date').text(moment().format('DD/MM/YYYY'));
      $('#txt').text(moment().format('HH : mm : ss'));
    }, 1000);
  }



</script>

</body>
</html>
