<?php
  if($_GET){
    $start =  $_GET["start"];
    $end = $_GET["end"];
  }
  include 'connect.php';
  // On connect
  $arr = array();
  $arr_in0 = array();
  $arr_in = array();
  $arr_put = array();
  $i=0;
  $in0=0;
  $in=0;

    $sql = "SELECT t1.empn,t1.title,t1.fname,t1.surname,t1.tel,t1.section2,t2.type
             FROM hr_dbo_view_card t1
             INNER JOIN taa_staff t2
             ON (t1.staff = t2.id)
             WHERE t1.staff != 0
             GROUP BY t1.empn";

    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $arr_in0[$in0] = array(
                                 "empn"=>$row['empn'],
                                 "title"=>$row['title'],
                                 "fname"=>$row['fname'],
                                 "surname"=>$row['surname'],
                                 "tel"=>$row['tel'],
                                 "section2"=>$row['section2'],
                                 "type"=>$row['type']
                          );
             $in0++;

        }
    } else {
        echo "0 results";
    }
    // -------------------------------------------------------------------------------------------
    $sql1 = "SELECT t1.empn,t1.title,t1.fname,t1.surname,t1.tel,t1.section2,t4.TIMESTAMP,t4.IP,t5.type
             FROM hr_dbo_view_card t1
             LEFT JOIN taa_card t2
             ON (t1.empn = t2.empn)
             LEFT JOIN taa_finger t3
             ON (t1.empn = t3.empn)
             INNER JOIN taa_logs t4
             ON (t2.CARD = t4.CARD) or (t3.fingerid = t4.CARD)
             INNER JOIN taa_staff t5
             ON (t1.staff = t5.id)
             WHERE TIMESTAMP BETWEEN '".$start."' AND '".$end."'
             AND t1.staff != 0
             -- GROUP BY t1.empn
             -- ORDER BY t4.TIMESTAMP DESC
             ORDER BY t1.empn DESC

             ";

    $result = $conn->query($sql1);
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $arr_in[$in] = array(
                                 "empn"=>$row['empn'],
                                 "title"=>$row['title'],
                                 "fname"=>$row['fname'],
                                 "surname"=>$row['surname'],
                                 "section2"=>$row['section2'],
                                 "date"=>$row['TIMESTAMP'],
                                 "ip"=>$row['IP'],
                                 "tel"=>$row['tel'],
                                 "type"=>$row['type']
                          );
             $in++;

        }
    } else {
        echo "0 results";
    }



  $conn->close();
  $arr_put = array("List0"=>$arr_in0,"List"=>$arr_in);
  echo (json_encode($arr_put)) ;
?>
