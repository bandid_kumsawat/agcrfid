<?php
  include 'connect.php';
  // On connect
  $arr = array();
  $arr1 = array();
  $arr_put = array();
  $i=0;
  $j=0;
  $sql = "SELECT t1.empn,t2.CARD,t1.title,t1.fname,t1.surname,t1.section2,t1.fay,t1.long,t1.team,t1.tel,t3.type,t4.fingerid
          FROM hr_dbo_view_card t1
          INNER JOIN taa_card t2
          ON (t1.empn = t2.empn)
          LEFT JOIN taa_staff t3
          ON (t1.staff = t3.id)
          LEFT JOIN taa_finger t4
          ON (t1.empn = t4.empn)
          WHERE t3.id != 0";

  $result = $conn->query($sql);
  if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {

          $arr[$i] = array(
                             "empn"=>$row['empn'],
                             "card"=>$row['CARD'],
                             "finger"=>$row['fingerid'],
                             "title"=>$row['title'],
                             "fname"=>$row['fname'],
                             "surname"=>$row['surname'],
                             "section2"=>$row['section2'],
                             "fay"=>$row['fay'],
                             "long"=>$row['long'],
                             "team"=>$row['team'],
                             "tel"=>$row['tel'],
                             "staff"=>$row['type']

                         );
           $i++;
      }
  } else {
      echo "0 results";
  }
// -----------------------------------------------------------------
  $sql1 = "SELECT * FROM taa_staff";

  $result = $conn->query($sql1);
  if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {

          $arr1[$j] = array(
                             "id"=>$row['id'],
                             "type"=>$row['type']
                         );
           $j++;
      }
  } else {
      echo "0 results";
  }
  
  $conn->close();
  $arr_put = array("Total"=>$i,"List"=>$arr,"List_s"=>$arr1);
  echo (json_encode($arr_put)) ;
?>
