<?php
  include 'connect.php';
  // On connect
  $arr = array();
  $arr_put = array();
  $i=0;
  $sql = "SELECT *
          FROM taa_alert";
  $result = $conn->query($sql);
  if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {

          $arr[$i] = array(
                             "id"=>$row['id'],
                             "card"=>$row['card'],
                             "title"=>$row['title'],
                             "empn"=>$row['empn'],
                             "fname"=>$row['fname'],
                             "surname"=>$row['surname'],
                             "message"=>$row['message'],
                             "date"=>$row['date']
                         );
           $i++;
      }
  } else {
      echo "0 results";
  }
  $conn->close();
  $arr_put = array("Total"=>$i,"List"=>$arr);
  echo (json_encode($arr_put)) ;
?>
