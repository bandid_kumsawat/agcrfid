<?php
session_start();
$user = $_SESSION['username'];
// $api_url=$_SESSION['url'];
if( (!isset($_SESSION['valid'])) || ($_SESSION['valid']!=1)){
  header('Location: ../index.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>AGC-RFID</title>
  <meta http-equiv=Content-Type content="text/html; charset=utf-8">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../CSS/bootstrap.min.css">
  <link rel="stylesheet" href="../CSS/fonts.css">
  <link rel="stylesheet" href="../CSS/fontawesome-all.css">
  <link rel="stylesheet" href="../CSS/fontawesome-all.min.css">
  <link rel="stylesheet" href="../CSS/sidenav.css">
  <link rel="stylesheet" href="../CSS/style.css">
  <link rel="stylesheet" href="../CSS/datatables.css">
  <link rel="stylesheet" href="../CSS/autocomplete.css">
  <link rel="stylesheet" type="text/css" href="../CSS/jquery.datetimepicker.css"/>
  <style type="text/css">

  </style>
</head>
<body>
  <!-- As a heading -->
<div id="nav"></div>
<br><br><br><br>
<div class="container-fluid">
  <div class="row">
    <div class="col-lg-2 col-md-2"></div>
    <div class="col-lg-10 col-md-10">

      <div class="panel panel-body card" id="main_panel">
        <br>
        <div class="container-fluid ">

          <h1><i class="far fa-bell"></i> ตั้งค่าการแจ้งเตือน</h1>
          <hr>
          <table class="table  table-bordered " id="add_table">
            <thead align="center">
              <tr class="tr_head tr_color" >
                <th scope="col" width="10%">รหัสพนักงาน</th>
                <th scope="col" width="10%">Card</th>
                <th scope="col" width="10%">ชื่อต้น</th>
                <th scope="col" width="10%">ชื่อ</th>
                <th scope="col" width="10%">นามสกุล</th>
                <th scope="col" width="15%">วันที่</th>
                <th scope="col" width="40%">ประกาศ</th>
                <th scope="col" ></th>
              </tr>
            </thead>

              <tr>
                <form action="./api/data_alert.php" method="post">
                  <tr class="tr_body" >
                    <td width="10%"><div class="autocomplete" style="width:100px;">
                      <input type="text" class="form-control" id="id" name="id" value="">
                    </div></td>
                    <td width="10%"><input type="text" class="form-control" name="card" id="card" value="" ></td>
                    <td width="5%"><input type="text" class="form-control" name="title" id="title" value="" ></td>
                    <td width="10%"><input type="text" class="form-control" name="name" id="name" value="" ></td>
                    <td width="10%"><input type="text" class="form-control" name="surname" id="surname" value="" ></td>
                    <td width="15%"><input type="text" class="form-control" name="date" id="alert_date" value=""></td>
                    <td width="40%"><textarea <input type="text" class="form-control" name="message" id="message" value=""></textarea></td>
                    <td align="center"><button id="add" name="add" type="submit" class="btn btn-primary btn-lg" onclick="sum_message()">เพิ่ม</button></td>
                  </tr>
                </form>
              </tr>
            </tbody>
            <br>
          </table>

          <br>
          <hr>
          <table class="table  table-bordered " id="show_table">
            <thead align="center">
              <tr class="tr_head tr_color" >
                <th scope="col" width="10%">รหัสพนักงาน</th>
                <th scope="col" width="10%">Card</th>
                <th scope="col" width="5%">ชื่อต้น</th>
                <th scope="col" width="10%">ชื่อ</th>
                <th scope="col" width="10%">นามสกุล</th>
                <th scope="col" width="15%">วันที่</th>
                <th scope="col" width="40%">ประกาศ</th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody id="body_t" align="center">

            </tbody>
            <br>
          </table>
        </div>
        <br>
        <div id="spinner">
              <center><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
              <span class="sr-only">Loading...</span></center>
        </div>
        <br>
      </div>
    </div>
  </div>
</div>
<!-- ======== UserName Modal ========-->
<div id="modal"></div>

<!-- JS -->
<script src="../JS/nav.js"></script>
<script src="../JS/jquery.min.js"></script>
<script src="../JS/bootstrap.min.js"></script>
<script src="../JS/datatables.js"></script>
<script src="../JS/jquery.datetimepicker.full.min.js"></script>
<script src="../JS/moment.min.js"></script>

<script type="text/javascript">

  var select=[];
  var input=[];
  var check;

  $(document).ready(function() {
    // nav
    make_nav();
    admin("<?php echo $_SESSION['username'];?>");
    user();
    // nav
    $('#spinner').show();
    try{
      show_alert();
      auto_detail();
    }catch(e){}


    autocomplete(document.getElementById("id"), select);

   setInterval(function(){
     $('#alert_date').val(moment().format('YYYY-MM-DD,HH:mm:ss'));
   }, 1000);
  });

  function make_nav(){

    nav("noti");

    $('#dd').append(
      '<td> '+"<?php echo $_SESSION['username'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['name'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['surname'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['email'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['phone'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['create_date'];?>"+' </td>'
    );

  }

  function show_alert(){
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "../pages/api/api_alert.php",
      "method": "GET"
    }

    $.ajax(settings).done(function (response) {
     // try{
        var decode = JSON.parse(response);
        console.log(decode);
    
        for(var i=0; i<decode.Total; i++){
          $('#body_t').append(
            '<tr class="tr_body">'+
              '<td width="10%" >'+decode.List[i].empn+'</td>'+
              '<td width="10%">'+decode.List[i].card+'</td>'+
              '<td width="10%">'+decode.List[i].title+'</td>'+
              '<td width="10%">'+decode.List[i].fname+'</td>'+
              '<td width="10%">'+decode.List[i].surname+'</td>'+
              '<td width="15%">'+decode.List[i].date+'</td>'+
              '<td width="40%">'+decode.List[i].message+'</td>'+
              '<td align="center">'+

                  '<div class="btn-group btn-group-lg">'+
                    '<form action="./api/data_alert.php" method="post">'+
                      '<input type="hidden" name="del" value="'+decode.List[i].id+'">'+
                      '<button id="del'+i+'"  type="submit" class="btn btn-danger btn-sm" style="display:none;"></button>'+
                    '</form>'+
                  '</div>'+
                  '<button id="del_confirm"  type="btn" class="btn btn-danger btn-md" onclick="confirm_del('+i+');">ลบ</button>'+
              '</td>'+
            '</tr>'
          );
        }
        $('#show_table').DataTable();
     // }catch{}
      $('#spinner').hide();
    });
  }

  function auto_detail(){
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "./api/api_taa_empm.php",
      "method": "GET"
    }

    $.ajax(settings).done(function (response) {
      var json = JSON.parse(response);
      for(var i=0; i<json.Total; i++){
        select.push(json.List[i].empn);
        input.push(json.List[i]);
      }

      console.log(select);
      // console.log(input);

    });
  }

  function autocomplete(inp, arr) {


    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function(e) {
        var a, b, i, val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) { return false;}
        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);
        /*for each item in the array...*/
        for (i = 0; i < arr.length; i++) {
          /*check if the item starts with the same letters as the text field value:*/
          if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
            /*create a DIV element for each matching element:*/
            b = document.createElement("DIV");
            /*make the matching letters bold:*/
            b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
            b.innerHTML += arr[i].substr(val.length);
            /*insert a input field that will hold the current array item's value:*/
            b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
            /*execute a function when someone clicks on the item value (DIV element):*/
                b.addEventListener("click", function(e) {
                /*insert the value for the autocomplete text field:*/
                inp.value = this.getElementsByTagName("input")[0].value;
                /*close the list of autocompleted values,
                (or any other open lists of autocompleted values:*/
                closeAllLists();
            });
            a.appendChild(b);
          }
        }
    });
    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function(e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
          /*If the arrow DOWN key is pressed,
          increase the currentFocus variable:*/
          currentFocus++;
          /*and and make the current item more visible:*/
          addActive(x);
        } else if (e.keyCode == 38) { //up
          /*If the arrow UP key is pressed,
          decrease the currentFocus variable:*/
          currentFocus--;
          /*and and make the current item more visible:*/
          addActive(x);
        } else if (e.keyCode == 13) {
          /*If the ENTER key is pressed, prevent the form from being submitted,*/
          e.preventDefault();
          if (currentFocus > -1) {
            /*and simulate a click on the "active" item:*/
            if (x) x[currentFocus].click();
          }
        }
    });
    function addActive(x) {
      /*a function to classify an item as "active":*/
      if (!x) return false;
      /*start by removing the "active" class on all items:*/
      removeActive(x);
      if (currentFocus >= x.length) currentFocus = 0;
      if (currentFocus < 0) currentFocus = (x.length - 1);
      /*add class "autocomplete-active":*/
      x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
      /*a function to remove the "active" class from all autocomplete items:*/
      for (var i = 0; i < x.length; i++) {
        x[i].classList.remove("autocomplete-active");
      }
    }
    function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
          if (elmnt != x[i] && elmnt != inp) {
          x[i].parentNode.removeChild(x[i]);
        }
      }
    }
    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
      closeAllLists(e.target);

      if($('#id').val() != "" && $('#id').val() != check){
        for(var i=0; i<input.length; i++){

          if($('#id').val() == input[i].empn){
            $('#card').val(input[i].card);
            $('#title').val(input[i].title);
            $('#name').val(input[i].fname);
            $('#surname').val(input[i].surname);

            break;
          }
        }
        check = $('#id').val();
      }
    });
  }

  function sum_message(){
    var mess = $('#message').val();
    var date = $('#alert_date').val();
    var obj = ''+mess+'\nวันที่ : '+date;
    $('#message').val(obj);
  }

  function confirm_del(index){

    if (confirm("ยืนยันการลบ?")) {
        $('#del'+index).click();
    }

  }


</script>

</body>
</html>
