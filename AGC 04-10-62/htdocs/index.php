<?php
  session_start();
  //$api_url=$_SESSION['url'];ดหฟกดฟหกดหก
?>

<!DOCTYPE html>
<html>
<title>เครื่องลงเวลา</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="./CSS/fonts.css">
<link rel="stylesheet" href="./CSS/fontawesome-all.css">
<link rel="stylesheet" href="./CSS/fontawesome-all.min.css">
<link rel="stylesheet" href="./CSS/login.css">
<link rel="stylesheet" type="text/css" href="./CSS/jquery.datetimepicker.css"/>

<body>

<div class="container">
    <div class="row vertical-offset-100">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-form shadow">
                <center><img src='./img/logo.png' width="80%" height="60%"></center>
               <!-- <h2 class="login_header"><b>LOGIN</b></h2> -->
               <br><br>
               <div class="form-group user-status">
                 <input type="text" class="form-control" placeholder="UserName " id="UserName" required>
                 <i class="fa fa-user"></i>
               </div>

               <div class="form-group log-status">
                 <input type="password" class="form-control" placeholder="Password" id="Password" required>
                 <i class="fa fa-lock"></i>
               </div>

                <span id="alert_txt" class="alert">Invalid Credentials</span>
                <!-- <a class="link" href="#">Lost your password?</a> -->
                <br><br>
               <button type="submit" class="log-btn" id="submit_button" >Log in</button>
             </div>
        </div>
    </div>
</div>
</body>


<!-- JS -->
<script src="./JS/jquery.min.js"></script>
<script>
const ENTER_KEY = 13;

$(document).keyup(function(e) {
     if (e.keyCode == ENTER_KEY) { // escape key maps to keycode `27`
        login();
    }
});

$('.log-btn').click(function(){
    login();
});

$('.form-control').keypress(function(){
    $('.log-status').removeClass('wrong-entry');
});



function login(){
    var user = $('#UserName').val();
    var pass = $('#Password').val();


    if(user ==""){
      $('.user-status').addClass('wrong-entry');
    }

    if(pass ==""){
      $('.log-status').addClass('wrong-entry');
    }

   if(user != '' && pass !=''){

      var form = new FormData();
      form.append("user",  user);
      form.append("pass", pass);

      var settings = {
        "async": true,
        "crossDomain": true,
        "url": "./admin/checklogin.php",
        "method": "POST",
        "processData": false,
        "contentType": false,
        "mimeType": "multipart/form-data",
        "data": form
      }

      $.ajax(settings).done(function (response) {

          console.log(response);
          console.log(parseInt(response));

          if( isNaN(parseInt(response)) ){
            $('.user-status').addClass('wrong-entry');
            $('.log-status').addClass('wrong-entry');
          }else{
            window.location.replace("./pages/emp.php");
          }
      });

    }
}



</script>
</html>
