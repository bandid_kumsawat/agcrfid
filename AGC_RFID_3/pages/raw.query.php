<?php
  $servername = "localhost";
  $username = "root";
  $password = "";
  $dbname = "taa_db";
  $conn = new mysqli($servername, $username, $password, $dbname);
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  }
  mysqli_set_charset($conn, 'utf8');
  // อยากดูวันไหนก็มาเปลี่ยนตรงนี้นะครับ วันที่เริ่มต้น $start, วันสิ้นสุดการค้นหา $end
  // ข้อมูลทั้งหมดเมื่อ query เสร็จอยู่ในตัวแปร $data 
  // ข้อมูลที่อยู่ตัวแปล $data จะไป echo ที่ตัวแปร var data ของ javascript เพื่อสร้างตาราง
  $start =  "2018-10-10 00:00:00";
  $end =    "2018-10-10 23:59:59";
  include './api/connect.php';
  $arr = array();
  $arr_in = array();
  $arr_put = array();
  $i=0;
  $in=0;

    $sql1 = "SELECT t1.empn,t1.title,t1.fname,t1.surname,t2.CARD,t4.IP,t4.TIMESTAMP,t4.SERVERTIME,t4.CARD as CARD_log
              FROM hr_dbo_view_card t1
              LEFT JOIN taa_card t2
              ON (t1.empn = t2.empn)
              LEFT JOIN taa_finger t3
              ON (t1.empn = t3.empn)
              INNER JOIN taa_logs t4
              ON (t3.fingerid = t4.CARD) or (t2.CARD = t4.CARD)
              WHERE TIMESTAMP BETWEEN '".$start."' AND '".$end."'
              ORDER BY t4.TIMESTAMP DESC";

    $result = $conn->query($sql1);
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $arr_in[$in] = array(
                                 "empn"=>$row['empn'],
                                 "title"=>$row['title'],
                                 "fname"=>$row['fname'],
                                 "surname"=>$row['surname'],
                                 "date"=>$row['TIMESTAMP'],
                                 "IP"=>$row['IP'],
                                 "card"=>$row['CARD'],
                                 "date_serv"=>$row['SERVERTIME'],
                                 "card_log"=>$row['CARD_log']
                          );
             $in++;

        }
    } else {
        echo "0 results";
    }

  // }

  $conn->close();
  $arr_put = array("Total"=>$in,"List"=>$arr_in);
  $data = json_encode($arr_put);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>AGC-RFID</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../CSS/bootstrap.min.css">
  <link rel="stylesheet" href="../CSS/fonts.css">
  <link rel="stylesheet" href="../CSS/fontawesome-all.css">
  <link rel="stylesheet" href="../CSS/fontawesome-all.min.css">
  <link rel="stylesheet" href="../CSS/sidenav.css">
  <link rel="stylesheet" href="../CSS/style.css">
  <link rel="stylesheet" href="../CSS/datatables.css">
  <link rel="stylesheet" type="text/css" href="../CSS/jquery.datetimepicker.css"/>
  <style type="text/css">

  </style>
</head>
<body>

<br><br><br><br>
<div class="container-fluid">
  <div class="row">
    <div class="col-lg-2 col-md-2"></div>
    <div class="col-lg-10 col-md-10">
    <div class="panel panel-body card" id="main_panel">
      <div class="container-fluid ">
        <br>
        <div class="row">

        </div>
        <hr>
        <div class="container-fluid" id="head_t">
          <table class="table" id="main_table">
            <thead>
              <tr class="tr_head tr_color">
                <th scope="col">รหัสพนักงาน</th>
                <th scope="col">ชื่อต้น</th>
                <th scope="col">ชื่อ</th>
                <th scope="col">นามสกุล</th>
                <th scope="col">IP</th>
                <th scope="col">วิธีลงเวลา</th>
                <th scope="col">วันที่-เวลา</th>
              </tr>
            </thead>
            <tbody id="body_t">
              <!-- insert_data -->
            </tbody>
          </table>
        </div>
        <br>
        <div id="spinner">
            <center><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
            <span class="sr-only">Loading...</span></center>
        </div>
        <br>
      </div>
    </div>
  </div>
</div>
</div>

<!-- JS -->
<script src="../JS/nav.js"></script>
<script src="../JS/jquery.min.js"></script>
<script src="../JS/bootstrap.min.js"></script>
<script src="../JS/jquery.datetimepicker.full.min.js"></script>
<script src="../JS/datatables.js"></script>
<script src="../JS/moment.min.js"></script>

<script src="../JS/underscore-min.js"></script>
<script src="../JS/pdfmake.min.js"></script>
<script src="../JS/vfs_fonts.js"></script>
<script src="../JS/download.js"></script>

<script type="text/javascript">

  var data,st_date,end_date;
  var g_table;

  var d_in = ['172.16.73.150','172.16.73.152','172.16.73.154'];
  var d_out = ['172.16.73.151','172.16.73.153','172.16.73.155','172.16.73.156'];

  $(document).ready(function() {
    try {
        var data = JSON.parse('<?php echo $data;?>')
        console.log(data);
        insert_data_n(data);
    } catch (e) {
        // console.log(e);
        alert('ไม่พบข้อมูลasdf');
        $('#spinner').hide();
    }

  });
  function insert_data_n(data_j){
      var te = 0;
      console.log("!!! Raw !!!");
      console.log(data_j);
      _.each(data_j.List, function(val){
        console.log(val.card_log);
        $('#body_t').append(
            // '<tr class="tr_body">'+
            '<tr  class="tr_body">'+
              '<th scope="row">'+ val.empn +'</th>'+
              '<td >'+val.title+'</td>'+
              '<td>'+val.fname+'</td>'+
              '<td>'+val.surname+'</td>'+
              '<td>'+check_ip(val.IP)+'</td>'+
              '<td>'+check_active(val.card_log)+'</td>'+
              '<td>'+val.date+'</td>'+
            '</tr>'
        );
      });  
      
    
      // --------------------------- Data Table -----------------------
      $('#main_table').DataTable({
            dom: 'Bfrtip',
            buttons: [
              {
                    text: 'TAF',
                    action: function ( e, dt, button, config ) {
                        var data = dt.buttons.exportData();
                        map_data(data);
                    }
                }
            ]
          });
      $('#spinner').hide();
      mainclick();
      // --------------------------- Data Table -----------------------
  }
  
  function calculate_time(){

    var table = $("#body_t");

    table.find('tr').each(function (i) {
       var $tds = $(this).find('td'),
           productId = $tds.eq(3).text(),
           a =  moment($tds.eq(4).text(),"HH:mm:ss"),
           b =  moment($tds.eq(5).text(),"HH:mm:ss"),
           sum = b.diff(a,"second"),
           formatted = moment.utc(sum*1000).format('HH:mm' + " ชม.");
           console.log(sum);
           $tds.eq(6).text(formatted);
   });

  }

  function map_data(data){

    var ms = data.body;
    console.log(ms);

    var auto_content='';
    var text;


      for(var j=0;j<ms.length;j++){
        if(ms[j][5] != "-"){
          if(ms[j][4] == 'เครื่องเข้า'){
            text = ms[j][0]+'   I '+taf_date(filter_date(ms[j][6],0))+' '+taf_time(filter_date(ms[j][6],1))+' 01';
            auto_content = auto_content+text+'\r\n';
          }
          if(ms[j][4] == 'เครื่องออก'){
            text = ms[j][0]+'   O '+taf_date(filter_date(ms[j][6],0))+' '+taf_time(filter_date(ms[j][6],1))+' 02';
            auto_content = auto_content+text+'\r\n';
          }
        }
      }

      pdfMake.fonts = {
        THSarabunNew: {
          normal: 'THSarabunNew.ttf',
          bold: 'THSarabunNew-Bold.ttf',
          italics: 'THSarabunNew-Italic.ttf',
          bolditalics: 'THSarabunNew-BoldItalic.ttf'
        },
        Roboto: {
          normal: 'Roboto-Regular.ttf',
          bold: 'Roboto-Medium.ttf',
          italics: 'Roboto-Italic.ttf',
          bolditalics: 'Roboto-MediumItalic.ttf'
        }
      }

      var docDefinition = {

        content : auto_content,

        defaultStyle:{
          font:'THSarabunNew'
        },

      };

      console.log(auto_content);

      download(auto_content, "Raw.TAF", "text/plain");

  }

  // ----------------------------date TAF---------------

  function filter_date(date,select){
    if(select == 0){
      var date = date.split(" ")[0];
      // console.log(date);
      return date;
    }
    else if(select == 1){
      var time = date.split(" ")[1];
      // console.log(time);
      return time;
    }
    else{
      return "-";
    }

  }

  function taf_date(date){
    var data = date.split('-');
    return ''+data[0]+data[1]+data[2]
  }

  function taf_time(date){
    var data = date.split(':');
    return ''+data[0]+data[1]
  }

  // ----------------------------Check IP----------------
  function check_ip(ip){
    for(var ip_position=0; ip_position<d_in.length; ip_position++){
      if(ip == d_in[ip_position]){
        return 'เครื่องเข้า';
        break;
      }
    }
    for(var ip_position1=0; ip_position1<d_out.length; ip_position1++){
      if(ip == d_out[ip_position1]){
        return 'เครื่องออก';
        break;
      }
    }
  }

  // ------------------------Check Active----------------
  function check_active(card){
    var res = card.substring(0, 2);
    if(res == "FN")
      return "ลายนิ้วมือ";
    else
      return "บัตรพนักงาน";
  }




</script>

</body>
</html>
