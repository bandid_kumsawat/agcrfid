<?php
  session_start();
  $user = $_SESSION['username'];
  //$api_url=$_SESSION['url'];
  if( (!isset($_SESSION['valid'])) || ($_SESSION['valid']!=1)){
    header('Location: ../index.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>AGC-RFID</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../CSS/bootstrap.min.css">
  <link rel="stylesheet" href="../CSS/fonts.css">
  <link rel="stylesheet" href="../CSS/fontawesome-all.css">
  <link rel="stylesheet" href="../CSS/fontawesome-all.min.css">
  <link rel="stylesheet" href="../CSS/sidenav.css">
  <link rel="stylesheet" href="../CSS/style.css">
  <link rel="stylesheet" href="../CSS/datatables.css">
  <link rel="stylesheet" href="../CSS/autocomplete.css">
  <style type="text/css">

  </style>
</head>
<body>
<!-- As a heading -->
<div id="nav"></div>
<br><br><br><br>
<div class="container-fluid">
  <div class="row">
    <div class="col-lg-2 col-md-2"></div>
    <div class="col-lg-10 col-md-10">
      <div class="panel panel-body card" id="main_panel">
        <br>
        <div class="container-fluid ">

          <h1 style="margin-left:20px"><i class="fas fa-user-plus"></i> จัดการพนักงาน</h1>
          <hr>
          <div class="col-md-12 col-lg-12">
            <form action="./api/data_emp.php" method="post">
            <div class="row">
              <div class="col-md-4 col-lg-4">
                <table class="table table-striped table-hover table-bordered">
                  <thead align="center">
                  <tr>
                    <th class="tr_head tr_color">รหัสพนักงาน</th>
                    <td class="tr_body"><input type="text" class="form-control" id="id" name="id" value=""></td>
                  </tr>
                  <tr>
                    <th class="tr_head tr_color">CARD</th>
                    <td class="tr_body"><input type="text" class="form-control" id="card" name="card" value=""></td>
                  </tr>

                  <tr>
                    <th class="tr_head tr_color">Finger</th>
                    <td class="tr_body"><input type="text" class="form-control" id="Finger" name="Finger" value=""></td>
                  </tr>

                  <tr>
                    <th class="tr_head tr_color">ชื่อต้น</th>
                    <td>
                      <select type="text" class="form-control" id="inname" name="title" value="">
                        <option value=""></option>
                        <option value="นาย">นาย</option>
                        <option value="นาง">นาง</option>
                        <option value="นางสาว">นางสาว</option>
                        <option value="">-</option>
                      </select>
                    </td>
                  </tr>
                  <tr>
                    <th class="tr_head tr_color">ชื่อ</th>
                    <td class="tr_body"><input type="text" class="form-control" id="name" name="name" value=""></td>
                  </tr>
                  <tr>
                    <th class="tr_head tr_color">นามสกุล</th>
                    <td class="tr_body"><input type="text" class="form-control"id="lastname"  name="surname" value=""></td>
                  </tr>


                  </thead>
                </table>
              </div>
              <div class="col-md-4 col-lg-4">
                <table class="table table-striped table-hover table-bordered">
                  <thead align="center">
                  <tr>
                    <th class="tr_head tr_color">แผนก</th>
                    <td class="tr_body"><input type="text" class="form-control" name="section" value=""></td>
                  </tr>
                  <tr>
                    <th class="tr_head tr_color">ฝ่าย</th>
                    <td class="tr_body"><input type="text" class="form-control" name="fay" value=""></td>
                  </tr>
                  <tr>
                    <th class="tr_head tr_color">ตำแหน่ง</th>
                    <td class="tr_body"><input type="text" class="form-control" name="long" value=""></td>
                  </tr>
                  <tr>
                    <th class="tr_head tr_color">สังกัด</th>
                    <td class="tr_body"><input type="text" class="form-control" name="team" value=""></td>
                  </tr>
                  <tr>
                    <th class="tr_head tr_color">เบอร์โทร</th>
                    <td class="tr_body"><input type="text" class="form-control"  name="tel" value=""></td>
                  </tr>
                  </thead>
                </table>
              </div>
              <div class="col-md-4 col-lg-4">
                <table class="table table-striped table-hover table-bordered">
                  <thead align="center">
                  <tr>
                    <th class="tr_head tr_color">พนักงานพิเศษ</th>
                    <td>
                      <select type="text" class="form-control" name="staff" id="staff_input" value="">
                        <option value="0" selected>พนักงาน</option>
                        <!-- <option value="1">พนักงานดับเพลิง</option> -->
                      </select>
                    </td>
                  </tr>
                  </thead>
                </table>
                <button id="add" name="add" type="submit" class="btn btn-primary btn-lg btn-block">เพิ่ม</button>
                </form>
                <br>
                <form action="upload.php" method="post" enctype="multipart/form-data">
                  <input type="file" name="fileToUpload" id="fileToUpload">
                  <button type="submit" name="submit" class="btn btn-primary btn-lg" id="upload">Upload</button>
                </form>

              </div>
            </div>
          </div>
          <div class="col-md-12 col-lg-12">

          </div>


          <br>
          <hr>
          <table class="table table-bordered " id="show_table">
            <thead align="center">
              <tr class="tr_head tr_color" >
                <th scope="col">รหัสพนักงาน</th>
                <th scope="col">Card</th>
                <th scope="col">Finger</th>
                <th scope="col">ชื่อต้น</th>
                <th scope="col">ชื่อ</th>
                <th scope="col">นามสกุล</th>
                <th scope="col">แผนก</th>
                <th scope="col">ฝ่าย</th>
                <th scope="col">ตำแหน่ง</th>
                <th scope="col">เบอร์โทร</th>
                <th scope="col">พนักงานพิเศษ</th>
                <th scope="col">ลบ</th>
              </tr>
            </thead>
            <tbody id="body_t" align="center">

            </tbody>
            <br>
          </table>
        </div>
        <br>
        <div id="spinner">
              <center><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
              <span class="sr-only">Loading...</span></center>
        </div>
        <br>
      </div>
    </div>
  </div>
</div>



<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="modal" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content" id="modal_content">
      <div class="modal-header"></div>
      <div class="modal-body"></div>
      <div class="modal-footer"></div>
    </div>
  </div>
</div>

<!-- JS -->
<script src="../JS/nav.js"></script>
<script src="../JS/jquery.min.js"></script>
<script src="../JS/bootstrap.min.js"></script>
<script src="../JS/datatables.js"></script>

<script type="text/javascript">

  var decode;
  var sp='<option value="0" selected>พนักงาน</option>';
  $(document).ready(function() {

    check_input();
    set_staff();
    // var a = $('#ID').val();
    // console.log(a);
    // nav
    make_nav();
    admin("<?php echo $_SESSION['username'];?>");
    // nav
    $('#spinner').show();
    show_emp();
    mainclick();
    check_emp();

  });

  function make_nav(){
    nav("add","<?php echo $_SESSION['privilege'] ?>","<?php echo $_SESSION['name'] . " " . $_SESSION['surname'];?>");
  }

  function set_staff(){
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "../pages/api/api_addstaff.php",
      "method": "GET"
    }

    $.ajax(settings).done(function (response) {
      decode = JSON.parse(response);
      for(var i=1; i<decode.List_s.length; i++){
        sp = sp+'<option value="'+decode.List_s[i].id+'">'+decode.List_s[i].type+'</option>';
        $('#staff_input').append(
          '<option value="'+decode.List_s[i].id+'">'+decode.List_s[i].type+'</option>'
        );
      }
      console.log(sp);
    });


  }

  function show_emp(){
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "../pages/api/api_taa_empm.php",
      "method": "GET"
    }

    $.ajax(settings).done(function (response) {
      
      try {
        decode = JSON.parse(response);
        console.log(decode);
        for(var i=0; i<decode.Total; i++){
          $('#body_t').append(
            '<tr  class="tr_body" data-toggle="modal" data-target="#modal" value="'+decode.List[i].empn+'">'+
              '<td><B>'+decode.List[i].empn+'</B></td>'+
              '<td>'+decode.List[i].card+'</td>'+
              '<td>'+((decode.List[i].finger != null) ? decode.List[i].finger : '0')+'</td>'+
              '<td>'+decode.List[i].title+'</td>'+
              '<td>'+decode.List[i].fname+'</td>'+
              '<td>'+decode.List[i].surname+'</td>'+
              '<td>'+decode.List[i].section2+'</td>'+
              '<td>'+decode.List[i].fay+'</td>'+
              '<td>'+decode.List[i].long+'</td>'+
              //'<td>'+decode.List[i].team+'</td>'+
              '<td>'+decode.List[i].tel+'</td>'+
              '<td>'+decode.List[i].staff+'</td>'+

              '<td align="center">'+

                  '<div class="btn-group btn-group-lg">'+
                    '<form action="./api/data_emp.php" method="post">'+
                      '<input type="hidden" name="del" value="'+decode.List[i].empn+'">'+
                      '<button id="del'+i+'"  type="submit" class="btn btn-danger btn-sm" style="display:none;"></button>'+
                    '</form>'+
                  '</div>'+
                  '<button id="del_confirm"  type="btn" class="btn btn-danger btn-md" onclick="confirm_del('+i+');">ลบ</button>'+

              '</td>'+
            '</tr>'
          );
        }
      } catch (e) {
        alert('ไม่พบข้อมูล');
        $('#spinner').hide();
      }
      $('#show_table').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'excel'
        ]
    } );
      $('#spinner').hide();
    });
  }

  function mainclick(){
    var id;
    $(".table").on('click','tr',function(e){
      console.log($(this).attr('value'));
      id = $(this).attr('value');
      reset_modal();
      create_modal(id);
    });

  }

  function reset_modal(){
      var modal = document.getElementById("modal_content");

      modal.innerHTML=
      '<form action="./api/data_emp.php" method="post" enctype="multipart/form-data">'+
          '<div class="modal-header" id="modal_header">'+

          '</div>'+
          '<div class="row modal-body" id="modal_body">'+
          '</div>'+
          '<div class="modal-footer" id="modal_footer">'+
          '</div>'+
        '</form>';
  }

  function create_modal(id){
    for(var i=0; i<decode.Total; i++){
      if(decode.List[i].empn == id){
        // var sp='';
        // if(decode.List[i].staff == 'พนักงาน'){
        //   sp = '<option value="0" selected >พนักงาน</option>'+
        //        '<option value="1">พนักงานดับเพลิง</option>'
        // }
        // if(decode.List[i].staff == 'พนักงานดับเพลิง'){
        //   sp = '<option value="0">พนักงาน</option>'+
        //        '<option value="1" selected >พนักงานดับเพลิง</option>'
        // }
        // console.log(sp);

        var tt='<option value="นาย" selected >นาย</option>'+
               '<option value="นาง">นาง</option>'+
               '<option value="นางสาว">นางสาว</option>'+
               '<option value="">-</option>';
               
        if(decode.List[i].title == 'นาย'){
          tt = '<option value="นาย" selected >นาย</option>'+
               '<option value="นาง">นาง</option>'+
               '<option value="นางสาว">นางสาว</option>'+
               '<option value="">-</option>';
        }
        if(decode.List[i].title == 'นาง'){
          tt = '<option value="นาย">นาย</option>'+
               '<option value="นาง" selected>นาง</option>'+
               '<option value="นางสาว">นางสาว</option>'+
               '<option value="">-</option>';
        }
        if(decode.List[i].title == 'นางสาว'){
          tt = '<option value="นาย">นาย</option>'+
               '<option value="นาง">นาง</option>'+
               '<option value="นางสาว" selected>นางสาว</option>'+
               '<option value="">-</option>';
        }
        console.log(tt);
        $('#modal_header').append(
            '<h3 class="modal-title"><i class="fa fa-edit"></i> แก้ไขข้อมูล</h3>'+
            '<button type="button" class="close" data-dismiss="modal">&times;</button>'
        );


        var emp_num_loc = "../img/ID_IMG/"+id+".png";
        
        
        $('#modal_body').append(
          '<div class="col-md-12 col-lg-12">'+


          '<center><img onerror="image_err(this)" src="'+emp_num_loc+'" alt="< Picture IN >" width="200" height="300"></center>'+    
          '<br>'+



            
          '</div>'+
          '<div class="col-md-12 col-lg-12">'+
            '<table class="table table-striped table-hover table-bordered">'+
              '<thead align="center">'+
              '<tr>'+
                '<th class="tr_head tr_color">รหัสพนักงาน</th>'+
                '<input type="hidden" class="form-control" name="id" value="'+decode.List[i].empn+'">'+
                '<td class="tr_body"><input type="text" class="form-control" name="" value="'+decode.List[i].empn+'" disabled></td>'+
              '</tr>'+
              '<tr>'+
                '<th class="tr_head tr_color">Card</th>'+
                '<td class="tr_body"><input type="text" class="form-control" name="card" value="'+decode.List[i].card+'"></td>'+
              '</tr>'+
              '<tr>'+
                '<th class="tr_head tr_color">Finger</th>'+
                '<td class="tr_body"><input type="text" class="form-control" name="Finger" value="'+decode.List[i].finger+'"></td>'+
              '</tr>'+
              '<tr>'+
                '<th class="tr_head tr_color">ชื่อต้น</th>'+
                '<td>'+
                  '<select type="text" class="form-control" name="title" value="">'+
                    tt+
                  '</select>'+
                '</td>'+
              '</tr>'+
              '<tr>'+
                '<th class="tr_head tr_color">ชื่อ</th>'+
                '<td class="tr_body"><input type="text" class="form-control" name="name" value="'+decode.List[i].fname+'"></td>'+
              '</tr>'+
              '<tr>'+
                '<th class="tr_head tr_color">นามสกุล</th>'+
                '<td class="tr_body"><input type="text" class="form-control" name="surname" value="'+decode.List[i].surname+'"></td>'+
              '</tr>'+
              '<tr>'+
                '<th class="tr_head tr_color">เบอร์โทร</th>'+
                '<td class="tr_body"><input type="text" class="form-control" name="tel" value="'+decode.List[i].tel+'"></td>'+
              '</tr>'+
              '<tr>'+
                '<th class="tr_head tr_color">แผนก</th>'+
                '<td class="tr_body"><input type="text" class="form-control" name="section" value="'+decode.List[i].section2+'"></td>'+
              '</tr>'+
              '<tr>'+
                '<th class="tr_head tr_color">ฝ่าย</th>'+
                '<td class="tr_body"><input type="text" class="form-control" name="fay" value="'+decode.List[i].fay+'"></td>'+
              '</tr>'+
              '<tr>'+
                '<th class="tr_head tr_color">ตำแหน่ง</th>'+
                '<td class="tr_body"><input type="text" class="form-control" name="long" value="'+decode.List[i].long+'"></td>'+
              '</tr>'+
              '<tr>'+
                '<th class="tr_head tr_color">สังกัด</th>'+
                '<td class="tr_body"><input type="text" class="form-control" name="team" value="'+decode.List[i].team+'"></td>'+
              '</tr>'+
              '<tr>'+
                '<th class="tr_head tr_color">พนักงานพิเศษ</th>'+
                '<td>'+
                  '<select type="text" class="form-control" name="staff" value="">'+
                    sp+
                  '</select>'+
                '</td>'+
              '</tr>'+
              '<tr>'+
                '<th class="tr_head tr_color">อัพโหลดรูปภาพ</th>'+
                '<td>'+
                  '<input type="file" name="fileToUpload1" id="fileToUpload1">'+
                '</td>'+
              '</tr>'+
              '</thead>'+
            '</table>'+
           '</div>'
        );
        $('#modal_footer').append(
            '<button name="edit" type="submit" class="btn btn-warning btn-lg" value = "'+id+'">แก้ไข</button>'
        );
        break;
      }
    }

  }

  function autocomplete(inp, arr){


    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function(e) {
        var a, b, i, val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) { return false;}
        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);
        /*for each item in the array...*/
        for (i = 0; i < arr.length; i++) {
          /*check if the item starts with the same letters as the text field value:*/
          if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
            /*create a DIV element for each matching element:*/
            b = document.createElement("DIV");
            /*make the matching letters bold:*/
            b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
            b.innerHTML += arr[i].substr(val.length);
            /*insert a input field that will hold the current array item's value:*/
            b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
            /*execute a function when someone clicks on the item value (DIV element):*/
                b.addEventListener("click", function(e) {
                /*insert the value for the autocomplete text field:*/
                inp.value = this.getElementsByTagName("input")[0].value;
                /*close the list of autocompleted values,
                (or any other open lists of autocompleted values:*/
                closeAllLists();
            });
            a.appendChild(b);
          }
        }
    });
    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function(e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
          /*If the arrow DOWN key is pressed,
          increase the currentFocus variable:*/
          currentFocus++;
          /*and and make the current item more visible:*/
          addActive(x);
        } else if (e.keyCode == 38) { //up
          /*If the arrow UP key is pressed,
          decrease the currentFocus variable:*/
          currentFocus--;
          /*and and make the current item more visible:*/
          addActive(x);
        } else if (e.keyCode == 13) {
          /*If the ENTER key is pressed, prevent the form from being submitted,*/
          e.preventDefault();
          if (currentFocus > -1) {
            /*and simulate a click on the "active" item:*/
            if (x) x[currentFocus].click();
          }
        }
    });
    function addActive(x) {
      /*a function to classify an item as "active":*/
      if (!x) return false;
      /*start by removing the "active" class on all items:*/
      removeActive(x);
      if (currentFocus >= x.length) currentFocus = 0;
      if (currentFocus < 0) currentFocus = (x.length - 1);
      /*add class "autocomplete-active":*/
      x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
      /*a function to remove the "active" class from all autocomplete items:*/
      for (var i = 0; i < x.length; i++) {
        x[i].classList.remove("autocomplete-active");
      }
    }
    function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
          if (elmnt != x[i] && elmnt != inp) {
          x[i].parentNode.removeChild(x[i]);
        }
      }
    }
    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
      closeAllLists(e.target);

    });
  }

  function check_input(){

    $("#add").click(function(){
    var id = $("#id").val().length;
    if(id == ''){
      alert('กรุณากรอกรหัสพนักงาน');
      return false;
    }
    }); //the end function

    $("#add").click(function(){
    var card = $("#card").val().length;
    if(card == ''){
      alert('กรุณากรอก CARD');
      return false;
    }
    }); //the end function


    $("#add").click(function(){
    var inname = $("#inname").val().length;
    if(inname == ''){
      alert('กรุณากรอกเลือกชื่อต้น');
      return false;
    }
    }); //the end function


    $("#add").click(function(){
    var name = $("#name").val().length;
    if(name == ''){
      alert('กรุณากรอกชื่อ ');
      return false;
    }
    }); //the end function


    $("#add").click(function(){
    var lastname = $("#lastname").val().length;
    if(lastname == ''){
      alert('กรุณากรอกนามสกุล');
      return false;
    }
    }); //the end function

  }

  function confirm_del(index){

    if (confirm("ยืนยันการลบ?")) {
        $('#del'+index).click();
    }

  }

  function check_emp(){
    $('#upload').click(function(){

    });
  }
  function image_err(soure){
    soure.src = "../img/photo_ava.png";
  }

</script>

</body>
</html>
