<?php session_start();
  $user = $_SESSION['username'];
  //$api_url=$_SESSION['url'];
  if( (!isset($_SESSION['valid'])) || ($_SESSION['valid']!=1)){
    header('Location: ../index.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>AGC-RFID</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../CSS/bootstrap.min.css">
  <link rel="stylesheet" href="../CSS/fonts.css">
  <link rel="stylesheet" href="../CSS/fontawesome-all.css">
  <link rel="stylesheet" href="../CSS/fontawesome-all.min.css">
  <link rel="stylesheet" href="../CSS/sidenav.css">
  <link rel="stylesheet" href="../CSS/style.css">
  <link rel="stylesheet" href="../CSS/datatables.css">
  <link rel="stylesheet" type="text/css" href="../CSS/jquery.datetimepicker.css"/>
  <style type="text/css">

  </style>
</head>
<body>
  <!-- As a heading -->
  <div id="nav"></div>

<br><br><br><br>
<div class="container-fluid">
  <div class="row">
    <div class="col-lg-2 col-md-2"></div>
    <div class="col-lg-10 col-md-10">
    <div class="panel panel-body card" id="main_panel">
      <div class="container-fluid ">
        <br>
        <div class="row">
            <div class="col-lg-4 col-md-4">
              <br>
              <h1><i class="far fa-address-card" style="margin-left:30px"></i> ตารางเวลาเข้าออก</h1>
              <!-- <div class="vll"></div> -->
            </div>

              <div class="col-lg-5 col-md-5">
                <div class="vl"></div>
                <div class="row">
                  <div class="col-lg-5 col-md-5">
                    <div class="form-group">
                      <label for="exampleFormControlFile1">วันที่เริ่มการค้นหา</label>
                      <input type="text" class="form-control" placeholder="วันที่ค้นหา" id="start_date" value="">
                    </div>
                  </form>
                  </div>
                  <div class="col-lg-5 col-md-5">
                    <div class="form-group">
                      <label for="exampleFormControlFile1">วันที่สิ้นสุดการค้นหา</label>
                      <input type="text" class="form-control" placeholder="วันที่ค้นหา" id="end_date" value="">
                    </div>
                  </form>
                  </div>
                  <div class="col-lg-2 col-md-2">
                    <br>
                    <button id="search" name="search" type="submit" class="btn btn-primary btn-lg">ดูข้อมูล</button>
                  </div>

                </div>
              </div>

              <div class="col-lg-3 col-md-3">
                <div class="vl"></div>
                <div class="row">
                  <div class="col-lg-3 col-md-3"></div>
                  <div class="col-lg-6 col-md-6">
                      <h3 id="real_date"></h3>
                    <hr>
                      <h3 id='txt' ><h3>
                  </div>
                  <div class="col-lg-3 col-md-3"></div>
                </div>
              </div>

        </div>
        <hr>
        <div class="container-fluid" id="head_t">
          <table class="table table-bordered" id="main_table">
            <thead align="center">
              <tr class="tr_head tr_color" >
                <th scope="col">รหัสพนักงาน</th>
                <th scope="col">ชื่อต้น</th>
                <th scope="col">ชื่อ</th>
                <th scope="col">นามสกุล</th>
                <th scope="col">วันที่</th>
                <th scope="col">เวลาเข้า 1</th>
                <th scope="col">เวลาออก 1</th>
                <th scope="col">เวลาเข้า 2</th>
                <th scope="col">เวลาออก 2</th>
                <th scope="col">เวลาปฎิบัติงาน</th>
              </tr>
            </thead>
            <tbody id="body_t">
              <!-- insert_data -->
            </tbody>
          </table>
        </div>
        <br>
        <div id="spinner">
            <center><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
            <span class="sr-only">Loading...</span></center>
        </div>
        <br>
      </div>
    </div>
  </div>
</div>
</div>
<!-- ======== UserName Modal ========-->
<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="modal" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content" id="modal_content">
      <div class="modal-header" id = "modal_header"></div>
      <div class="modal-body" id = "modal_body"></div>
      <div class="modal-footer" id = "modal_footer"></div>
    </div>
  </div>
</div>

<!-- JS -->
<script src="../JS/config.js"></script>
<script src="../JS/nav.js"></script>
<script src="../JS/jquery.min.js"></script>
<script src="../JS/bootstrap.min.js"></script>
<script src="../JS/jquery.datetimepicker.full.min.js"></script>
<script src="../JS/datatables.js"></script>
<script src="../JS/moment.min.js"></script>

<script src="../JS/underscore-min.js"></script>
<script src="../JS/pdfmake.min.js"></script>
<script src="../JS/vfs_fonts.js"></script>
<script src="../JS/download.js"></script>

<script type="text/javascript">

  var data,st_date,end_date;
  var g_table;

  // var d_in1 = '172.16.73.150';
  // var d_in2 = '172.16.73.152';
  // var d_out1 = '172.16.73.151';
  // var d_out2 = '172.16.73.153';

  // var d_in = ['172.16.73.150','172.16.73.152','172.16.73.154'];
  // var d_out = ['172.16.73.151','172.16.73.153','172.16.73.155'];

  $(document).ready(function() {
    
    // nav
    make_nav();
    admin("<?php echo $_SESSION['username'];?>");
    user();
    // nav
    $('#real_date').text(moment().format('DD/MM/YYYY'));
    $('#txt').text(moment().format('HH : mm : ss'));
    time();

    $('#start_date').datetimepicker({
       timepicker:false,
       format:'Y-m-d'
    });
    $('#end_date').datetimepicker({
       timepicker:false,
       format:'Y-m-d'
    });
    //$('#start_date').val(moment().format('YYYY-MM-DD'));
    //$('#end_date').val(moment().format('YYYY-MM-DD'));
    $('#start_date').val('2018-10-10');
    $('#end_date').val('2018-10-10');

    try {
      search();
      $('#search').click();
    }
    catch(e) {

    }

  });

  function make_nav(){

    nav("emp","<?php echo $_SESSION['privilege'] ?>","<?php echo $_SESSION['name'] . " " . $_SESSION['surname'];?>");

    $('#dd').append(
      '<td> '+"<?php echo $_SESSION['username'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['name'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['surname'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['email'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['phone'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['create_date'];?>"+' </td>'
    );

  }

  function search(){
    $('#search').click(function(){
      $('#head_t').empty();
      $('#head_t').append(
        '<table class="table  table-bordered" id="main_table">'+
          '<thead>'+
            '<tr class="tr_head tr_color">'+
              '<th scope="col">รหัสพนักงาน</th>'+
              '<th scope="col">ชื่อต้น</th>'+
              '<th scope="col">ชื่อ</th>'+
              '<th scope="col">นามสกุล</th>'+
              '<th scope="col">วันที่</th>'+
              '<th scope="col">เวลาเข้า 1</th>'+
              '<th scope="col">เวลาออก 1</th>'+
              '<th scope="col">เวลาเข้า 2</th>'+
              '<th scope="col">เวลาออก 2</th>'+
              '<th scope="col">เวลาปฎิบัติงาน</th>'+
            '</tr>'+
          '</thead>'+
          '<tbody id="body_t">'+
            '<!-- insert_data -->'+
          '</tbody>'+
        '</table>'

      );


      $('#body_t').empty();

      //---------------------------------------------------------------------------
      st_date = $('#start_date').val()+" 00:00:00";
      end_date = $('#end_date').val()+" 23:59:59";
      $('#spinner').show();
      data_table();
    });
  }

  function data_table(){

    var settings = {
      "async": true,
      "crossDomain": true,

      "url": "../pages/api/api_taa_emp_n.php?start="+st_date+"&end="+end_date,
      "method": "GET"
    }

    $.ajax(settings).done(function (response) {
      // data = JSON.parse(response)
      try {
        data = JSON.parse(response)
        insert_data_n(data);
      } catch (e) {

        alert('ไม่พบข้อมูล');
        $('#spinner').hide();
      }
    });
  }

  function insert_data_n(data_j){
      var te = 0;
      var night_arr=[];
      var result_in_min,result_in_max,result_out_min,result_out_max;
      var temp_1 = _.sortBy(data_j.List,'date');
      var temp_2 = _.groupBy(temp_1,'empn');

      
      _.each(temp_2,function(val){
          te++;
          var temp_3 = _.groupBy(val,'IP');
          var data_in = [];
          for(var i=0; i<d_in.length; i++){
            data_in = _.union(data_in,temp_3[d_in[i]])
          }

          var data_out = [];
          for(var i=0; i<d_in.length; i++){
            data_out = _.union(data_out,temp_3[d_out[i]])
          }

          var data_in_sort = _.sortBy(data_in,"date");
          var data_out_sort = _.sortBy(data_out,"date");


          result_in_min = _.first(data_in_sort);
          result_in_max = _.last(data_in_sort);
          result_out_min = _.first(data_out_sort);
          result_out_max = _.last(data_out_sort);



          var emp_t = '-';
          var title_t = '-';
          var name_t = '-';
          var surname_t = '-';
          var date_t = '-';
          var hr_in_t = '-';
          var hr_out_t = '-';
          var hr_in_t1 = '-';
          var hr_out_t1 = '-';
          var status_t = '-';

          var data_in_table;
          try {
          //---Start try

            if(result_in_min !== undefined){

              if(result_in_min.date > result_out_max.date){
                night_arr.push(result_in_min['empn']);
              }else{
                work_on_day(result_in_min,result_in_max,result_out_min,result_out_max);
              }

            }else{
              emp_t   = result_out_min['empn'];
              title_t = result_out_min['title'];
              name_t  = result_out_min['fname'];
              surname_t = result_out_min['surname'];

              date_t  = filter_date(result_out_min['date'],0);
              // hr_out_t = filter_date(result_out_min['date'],1);
              hr_out_t1 = filter_date(result_out_max['date'],1);

              data_in_table = {
                                emp:emp_t,
                                title:title_t,
                                name:name_t,
                                surname:surname_t,
                                date:date_t,
                                hr_in:hr_in_t,
                                hr_out:hr_out_t,
                                hr_in1:hr_in_t1,
                                hr_out1:hr_out_t1,
                                status:status_t
                              };
              add_table(data_in_table);
            }

          //---end try
          } catch (e) {

          }

      });
      work_on_night(night_arr);

      // // --------------------------- Data Table -----------------------
      // $('#main_table').DataTable({
      //       dom: 'Bfrtip',
      //       buttons: [
      //         {
      //               text: 'TAF',
      //               action: function ( e, dt, button, config ) {
      //                   var data = dt.buttons.exportData();
      //                   map_data(data);
      //               }
      //           }
      //       ]
      //     });
      // $('#spinner').hide();
      // // --------------------------- Data Table -----------------------
  }

  function work_on_day(result_in_min,result_in_max,result_out_min,result_out_max){

    var emp_t = '-';
    var title_t = '-';
    var name_t = '-';
    var surname_t = '-';
    var date_t = '-';
    var hr_in_t = '-';
    var hr_out_t = '-';
    var hr_in_t1 = '-';
    var hr_out_t1 = '-';
    var status_t = '-';

    if(result_in_min !== undefined){
      emp_t   = result_in_min['empn'];
      title_t = result_in_min['title'];
      name_t  = result_in_min['fname'];
      surname_t = result_in_min['surname'];

        date_t  = filter_date(result_in_min['date'],0);
        hr_in_t = filter_date(result_in_min['date'],1);

        hr_out_t1 = filter_date(result_out_max['date'],1);

        if(result_out_min.date !== undefined){

          if(result_out_min.date < result_in_max.date){
            hr_in_t1 = filter_date(result_in_max['date'],1);

            if(result_out_max.date > result_in_max.date){
              hr_out_t = filter_date(result_out_min['date'],1);
            }
          }
        }
        else{
            emp_t   = result_in_min['empn'];
            title_t = result_in_min['title'];
            name_t  = result_in_min['fname'];
            surname_t = result_in_min['surname'];

            date_t  = filter_date(result_in_min['date'],0);
            // hr_out_t = filter_date(result_out_min['date'],1);
            hr_out_t1 = filter_date(result_in_max['date'],1);
        }

    }else{
        emp_t   = result_out_min['empn'];
        title_t = result_out_min['title'];
        name_t  = result_out_min['fname'];
        surname_t = result_out_min['surname'];

        date_t  = filter_date(result_out_min['date'],0);
        // hr_out_t = filter_date(result_out_min['date'],1);
        hr_out_t1 = filter_date(result_out_max['date'],1);
    }

      data_in_table = {
                        emp:emp_t,
                        title:title_t,
                        name:name_t,
                        surname:surname_t,
                        date:date_t,
                        hr_in:hr_in_t,
                        hr_out:hr_out_t,
                        hr_in1:hr_in_t1,
                        hr_out1:hr_out_t1,
                        status:status_t
                      };
      add_table(data_in_table);

  }

  function work_on_night(night){
      var str_night = '';
      for(var j=0; j<night.length; j++){
        if(j == (night.length-1))
          str_night = str_night+night[j];
        else
          str_night = str_night+night[j]+','
      }
      var start = st_date;
      var end = st_date;
      var g_emp;
      var darkmode,dark_1,result,darkside,darkside2;
      var result_first_min,result_fisrt_max,result_last_min,result_last_max;


      var emp_d = '-';
      var title_d = '-';
      var name_d = '-';
      var surname_d = '-';
      var date_d = '-';
      var hr_in_d = '-';
      var hr_out_d = '-';
      var hr_in_d1 = '-';
      var hr_out_d1 = '-';
      var status_d = '-';

      var settings = {
        "async": true,
        "crossDomain": true,
        "url": "api/api_taa_emp_night.php?start="+start+"&end="+end+"&empn="+str_night,
        "method": "GET"
      }

      $.ajax(settings).done( function (response) {
        console.log(response);
        data = JSON.parse(response);
        g_emp = _.groupBy(data.List,'empn');

        _.each(g_emp,function(val){
          darkmode = val
          dark_1 = _.groupBy(darkmode,'IP');

          var data_in = [];
          for(var i=0; i<d_in.length; i++){
            data_in = _.union(data_in,dark_1[d_in[i]])
          }

          var data_out = [];
          for(var i=0; i<d_in.length; i++){
            data_out = _.union(data_out,dark_1[d_out[i]])
          }

          var data_in_sort = _.sortBy(data_in,"date");
          var data_out_sort = _.sortBy(data_out,"date");


          result_first_min = _.first(data_in_sort); //first day in
          result_first_max = _.last(data_in_sort);
          result_last_min = _.first(data_out_sort);
          result_last_max = _.last(data_out_sort); //last day out

          if(result_first_min !== undefined){
                  emp_d   = result_first_min['empn'];
                  title_d = result_first_min['title'];
                  name_d  = result_first_min['fname'];
                  surname_d = result_first_min['surname'];

                  date_d  = filter_date(result_first_min['date'],0);
                  hr_in_d = filter_date(result_first_min['date'],1);

                  hr_out_d1 = filter_date(result_last_max['date'],1);

                  // if(result_out_min.date < result_first_max.date){
                  //   hr_in_d1 = filter_date(result_first_max['date'],1);

                  //   if(result_out_max.date > result_first_max.date){
                  //     hr_out_d = filter_date(result_last_min['date'],1);
                  //   }
                  // }
          }else{
            emp_d   = result_last_max['empn'];
            title_d = result_last_max['title'];
            name_d  = result_last_max['fname'];
            surname_d = result_last_max['surname'];

            date_d  = filter_date(result_last_max['date'],0);
            // hr_out_t = filter_date(result_out_min['date'],1);
            hr_out_d1 = filter_date(result_last_max['date'],1);
          }

          darkside = {
                      emp:emp_d,
                      title:title_d,
                      name:name_d,
                      surname:surname_d,
                      date:date_d,
                      hr_in:hr_in_d,
                      hr_out:hr_out_d,
                      hr_in1:hr_in_d1,
                      hr_out1:hr_out_d1,
                      status:status_d
                    };

          add_table(darkside);
          });

          // --------------------------- Data Table -----------------------
          $('#main_table').DataTable({
                dom: 'Bfrtip',
                buttons: [
                  {
                        text: 'TAF',
                        action: function ( e, dt, button, config ) {
                            var data = dt.buttons.exportData();
                            map_data(data);
                        }
                    }
                ]
              });
          $('#spinner').hide();
          // --------------------------- Data Table -----------------------

      });

  }

  function add_table(data){
    $('#body_t').append(
            
              "<tr class='tr_body' id = 'row_click' onclick = row_click(\'"+data.emp+","+data.title+","+data.name+","+data.surname+","+data.hr_in+","+data.hr_out+","+data.hr_in1+","+data.hr_out1+","+data.date+"\')>"+
                '<th scope="row">'+ data.emp +'</th>'+
                '<td >'+data.title+'</td>'+
                '<td>'+data.name+'</td>'+
                '<td>'+data.surname+'</td>'+
                '<td>'+data.date+'</td>'+
                '<td>'+data.hr_in+'</td>'+
                '<td>'+data.hr_out+'</td>'+
                '<td>'+data.hr_in1+'</td>'+
                '<td>'+data.hr_out1+'</td>'+
                '<td>'+data.status+'</td>'+
              '</tr>'
            
      );

    try {
      var table = $("#body_t");
      var a="-",b="-",sum,formatted,$tds;
      if(table != undefined){
        table.find('tr').each(function (i) {
          $tds = $(this).find('td');

          if($tds.eq(8).text() == "-"){
            a =  moment($tds.eq(4).text(),"HH:mm:ss");
            b =  moment($tds.eq(7).text(),"HH:mm:ss");

            sum = b.diff(a,"second");
            formatted = moment.utc(sum*1000).format('HH:mm' + " ชม.");
            if(isNaN(sum)){
              if(a._i == "-"){
                $tds.eq(8).text("-");
              }
              if(b._i == "-"){
                $tds.eq(8).text("-");
              }
            }
            else{
              if(sum >= 0){
                $tds.eq(8).text(formatted);
              }
              if(sum < 0){
                $tds.eq(8).text(formatted);
              }
            }
          }
        });
      }
    } catch (e) {}
  }



  function calculate_time(){

    var table = $("#body_t");

    table.find('tr').each(function (i) {
       var $tds = $(this).find('td'),
           productId = $tds.eq(3).text(),
           a =  moment($tds.eq(4).text(),"HH:mm:ss"),
           b =  moment($tds.eq(5).text(),"HH:mm:ss"),
           sum = b.diff(a,"second"),
           formatted = moment.utc(sum*1000).format('HH:mm' + " ชม.");
           $tds.eq(6).text(formatted);
   });

  }

  function map_data(data){

    var ms = data.body;

    var ret_day = _.groupBy(ms, function(data){
      return data[4];
    });


    var arr = Object.keys(ret_day)
    var arr_date = _.sortBy(arr,function(input){return input;});


    // var auto_content = Array();
    var auto_content='';
    var p_break="";
    var text;

    for(var i=0; i< arr_date.length; i++){

      var  p_brake='';
      if(i!=0){
        p_brake = 'before'
      }


      for(var j=0;j<ret_day[arr_date[i]].length;j++){

        if(ret_day[arr_date[i]][j][5] != "-"){
          text = ret_day[arr_date[i]][j][0]+'   I '+taf_date(ret_day[arr_date[i]][j][4])+' '+taf_time(ret_day[arr_date[i]][j][5])+' 01';
          auto_content = auto_content+text+'\r\n';
        }
        if(ret_day[arr_date[i]][j][6] != "-"){
          text = ret_day[arr_date[i]][j][0]+'   O '+taf_date(ret_day[arr_date[i]][j][4])+' '+taf_time(ret_day[arr_date[i]][j][6])+' 01';
          auto_content = auto_content+text+'\r\n';
        }
        if(ret_day[arr_date[i]][j][7] != "-"){
          text = ret_day[arr_date[i]][j][0]+'   I '+taf_date(ret_day[arr_date[i]][j][4])+' '+taf_time(ret_day[arr_date[i]][j][7])+' 02';
          auto_content = auto_content+text+'\r\n';
        }
        if(ret_day[arr_date[i]][j][8] != "-"){
          text = ret_day[arr_date[i]][j][0]+'   O '+taf_date(ret_day[arr_date[i]][j][4])+' '+taf_time(ret_day[arr_date[i]][j][8])+' 02';
          auto_content = auto_content+text+'\r\n';
        }

      }


    }

      pdfMake.fonts = {
        THSarabunNew: {
          normal: 'THSarabunNew.ttf',
          bold: 'THSarabunNew-Bold.ttf',
          italics: 'THSarabunNew-Italic.ttf',
          bolditalics: 'THSarabunNew-BoldItalic.ttf'
        },
        Roboto: {
          normal: 'Roboto-Regular.ttf',
          bold: 'Roboto-Medium.ttf',
          italics: 'Roboto-Italic.ttf',
          bolditalics: 'Roboto-MediumItalic.ttf'
        }
      }

      var docDefinition = {

        content : auto_content,

        defaultStyle:{
          font:'THSarabunNew'
        },

      };

      // pdfMake.createPdf(docDefinition).download('test.txt');


      download(auto_content, "Export.TAF", "text/plain");

  }

  // ----------------------------date TAF---------------

  function filter_date(date,select){
    if(select == 0){
      var date = date.split(" ")[0];
      return date;
    }
    else if(select == 1){
      var time = date.split(" ")[1];
      return time;
    }
    else{
      return "-";
    }

  }

  function taf_date(date){
    var data = date.split('-');
    return ''+data[0]+data[1]+data[2]
  }

  function taf_time(date){
    var data = date.split(':');
    return ''+data[0]+data[1]
  }

  // ----------------------------time-------------------
  function time(){
    setInterval(function(){
      $('#real_date').text(moment().format('DD/MM/YYYY'));
      $('#txt').text(moment().format('HH : mm : ss'));
    }, 1000);
  }

  function reset_modal(){
      var modal = document.getElementById("modal");

      
  }
  function create_modal(id){

  }
  mainclick();    
  function mainclick(){
    var id;
    $(".table").on('click','tr',function(e){
      id = $(this).attr('value');
      /*reset_modal();
      create_modal(id);*/
    });

  }
  function row_click(val){
    console.log(val.split(","));
    reset_modal();
    // format :: 000993482824-09-2018-17_09_05.jpg   2790
    //           001179353610-10-2018-17_01_05.jpg   2790


    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "../pages/api/api_snap.php?emp="+val.split(",")[0]

      +"&in1="+val.split(",")[4].replace(":", "_").replace(":", "_").replace(":", "_").substring(0,5)
      +"&out1="+val.split(",")[5].replace(":", "_").replace(":", "_").replace(":", "_").substring(0,5)
      +"&in2="+val.split(",")[6].replace(":", "_").replace(":", "_").replace(":", "_").substring(0,5)
      +"&out2="+val.split(",")[7].replace(":", "_").replace(":", "_").replace(":", "_").substring(0,5)
      +"&date="+val.split(",")[8].replace(":", "-").replace(":", "_").replace(":", "_"),
      "method": "GET"
    }
    console.log(settings);
    $.ajax(settings).done(function (response) {

      try {
          
        reset_modal();
        var img_in1 = "photo_ava.png";
        var img_in2 = "photo_ava.png";
        var img_out1 = "photo_ava.png";
        var img_out2 = "photo_ava.png";
        data = JSON.parse(response);
        console.log("--------- api img ------------");
        console.log(data);
        console.log("------------------------------");
        if (data.count_in1 > 0){
          img_in1 = data.in1[0].substring(19,data.in1[0].length); 
        }else{
          img_in1 = "photo_ava.png";
        }

        if (data.count_in2 > 0){
          img_in2 = data.in2[0].substring(19,data.in2[0].length); 
        }else{
          img_in2 = "photo_ava.png";
        }

        if (data.count_out1 > 0){
          img_out1 = data.out1[0].substring(19,data.out1[0].length); 
        }else{
          img_out1 = "photo_ava.png";
        }

        if (data.count_out2 > 0){
          img_out2 = data.out2[0].substring(19,data.out2[0].length);
        }else{
          img_out2 = "photo_ava.png";
        }
        //img_out2 = data.out2[0].substring(19,500);
        console.log(img_in1);
        console.log(img_in2);
        console.log(img_out1);
        console.log(img_out2);

        
        document.getElementById("modal_header").innerHTML = 
        '<h5 class="modal-title" id="exampleModalLabel">รายงานพนักงาน</h5>'+
          '<button type="button" class="close" data-dismiss="modal" aria-label="Close">'+
          '<span aria-hidden="true">&times;</span>'+
        '</button>';

        document.getElementById("modal_body").innerHTML= 
          '<div class = "row">'+
              '<div class= "col-md-12">'+
              '<center>'+
                '<b>'+val.split(",")[1]+val.split(",")[2]+' '+val.split(",")[3]+'</b>'+
              '</center>'+
                '<center>'+
                  '<img src="../img/ID_IMG/'+val.split(",")[0]+'.png" alt="< Picture IN >" width="200" height="300">'+
                '</center>'+
              '</div>'+
          '</div>'+
          //  row 1
          '<div class = "row">'+
            '<div class = "col-md-6">'+
            '<center>'+
                '<b>Snap Shot เวลาเข้า 1</b>'+
              '</center>'+
              '<div class= "col-md-12">'+
                '<center>'+
                  '<img src="../img/SNAP_IMG/'+img_in1+'" alt="< Picture IN >" width="200" height="300">'+
                '</center>'+
              '</div>'+
            '</div>'+
            '<div class = "col-md-6">'+
            '<center>'+
              '<b>Snap Shot เวลาออก 1</b>'+
              '</center>'+
              '<div class= "col-md-12">'+
                '<center>'+
                  '<img src="../img/SNAP_IMG/'+img_in2+'" alt="< Picture IN >" width="200" height="300">'+
                '</center>'+
              '</div>'+
            '</div>'+
          '</div>'+
          //  row 1
          '<div class = "row">'+
            '<div class = "col-md-6">'+
            '<center>'+
                '<b>Snap Shot เวลาเข้า 2</b>'+
              '</center>'+
              '<div class= "col-md-12">'+
                '<center>'+
                  '<img src="../img/SNAP_IMG/'+img_out1+'" alt="< Picture IN >" width="200" height="300">'+
                '</center>'+
              '</div>'+
            '</div>'+
            '<div class = "col-md-6">'+
            '<center>'+
              '<b>Snap Shot เวลาออก 2</b>'+
              '</center>'+
              '<div class= "col-md-12">'+
                '<center>'+
                  '<img src="../img/SNAP_IMG/'+img_out2+'" alt="< Picture IN >" width="200" height="300">'+
                '</center>'+
              '</div>'+
            '</div>'+
          '</div>'
          ;

        document.getElementById("modal_footer").innerHTML= 
          '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>'


        $('#modal').modal('show');

      } catch (e) {
        console.log(e);
        document.getElementById("modal_header").innerHTML = 
        '<h5 class="modal-title" id="exampleModalLabel">รายงานพนักงาน</h5>'+
          '<button type="button" class="close" data-dismiss="modal" aria-label="Close">'+
          '<span aria-hidden="true">&times;</span>'+
        '</button>';

        document.getElementById("modal_body").innerHTML= 
          '<div class = "row">'+
              '<div class= "col-md-12">'+
              '<center>'+
                '<b>'+val.split(",")[1]+val.split(",")[2]+' '+val.split(",")[3]+'</b>'+
              '</center>'+
                '<center>'+
                  '<img src="../img/ID_IMG/'+val.split(",")[0]+'.png" alt="< Picture IN >" width="200" height="300">'+
                '</center>'+
              '</div>'+
          '</div>'+
          //  row 1
          '<div class = "row">'+
            '<div class = "col-md-6">'+
            '<center>'+
                '<b>Snap Shot เวลาเข้า 1</b>'+
              '</center>'+
              '<div class= "col-md-12">'+
                '<center>'+
                  '<img src="../img/SNAP_IMG/photo_ava.png" alt="< Picture IN >" width="200" height="300">'+
                '</center>'+
              '</div>'+
            '</div>'+
            '<div class = "col-md-6">'+
            '<center>'+
              '<b>Snap Shot เวลาออก 1</b>'+
              '</center>'+
              '<div class= "col-md-12">'+
                '<center>'+
                  '<img src="../img/SNAP_IMG/photo_ava.png" alt="< Picture IN >" width="200" height="300">'+
                '</center>'+
              '</div>'+
            '</div>'+
          '</div>'+
          //  row 1
          '<div class = "row">'+
            '<div class = "col-md-6">'+
            '<center>'+
                '<b>Snap Shot เวลาเข้า 2</b>'+
              '</center>'+
              '<div class= "col-md-12">'+
                '<center>'+
                  '<img src="../img/SNAP_IMG/photo_ava.png" alt="< Picture IN >" width="200" height="300">'+
                '</center>'+
              '</div>'+
            '</div>'+
            '<div class = "col-md-6">'+
            '<center>'+
              '<b>Snap Shot เวลาออก 2</b>'+
              '</center>'+
              '<div class= "col-md-12">'+
                '<center>'+
                  '<img src="../img/SNAP_IMG/photo_ava.png" alt="< Picture IN >" width="200" height="300">'+
                '</center>'+
              '</div>'+
            '</div>'+
          '</div>'
          ;

        document.getElementById("modal_footer").innerHTML= 
          '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>'


        $('#modal').modal('show');

      }
    });
  }
</script>

</body>
</html>
