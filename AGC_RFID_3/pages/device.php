<?php
session_start();
$user = $_SESSION['username'];
// $api_url=$_SESSION['url'];
if( (!isset($_SESSION['valid'])) || ($_SESSION['valid']!=1)){
  header('Location: ../index.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>AGC-RFID</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../CSS/bootstrap.min.css">
  <link rel="stylesheet" href="../CSS/fonts.css">
  <link rel="stylesheet" href="../CSS/fontawesome-all.css">
  <link rel="stylesheet" href="../CSS/fontawesome-all.min.css">
  <link rel="stylesheet" href="../CSS/sidenav.css">
  <link rel="stylesheet" href="../CSS/style.css">
  <link rel="stylesheet" href="../CSS/datatables.css">
  <style type="text/css">


  </style>
</head>
<body>
  <!-- As a heading -->
  <div id="nav"></div>
  <br><br><br><br>
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-2 col-md-2"></div>
      <div class="col-lg-10 col-md-10">
        <div class="panel panel-body card" id="main_panel">
          <div class="container-fluid ">
            <br>
            <div class="col-lg-12 col-md-12">
              <div class="row">
                <div class="col-lg-3 col-md-3">
                  <h1><i class="fas fa-tablet-alt"></i> ตารางอุปกรณ์</h1>
                </div>
                <div class="col-lg-6 col-md-6"></div>
                <div class="col-lg-3 col-md-3">
                  <!-- <form action="http://192.168.2.32/database/add_data_file.php" method="POST"  enctype="multipart/form-data"> -->
                  <form id="multiple_ip" action="" method="POST">
                    <div class="input-group mb-3">
                      <div class="custom-file">
                        <input type="file" id="file_upload" name="file_upload" accept=".db" />
                        <label class="custom-file-label" for="file_upload">Choose file</label>
                      </div>
                      <div class="input-group-prepend ml-1">
                        <input type='button' name='Submit' value='Submit' class="btn btn-outline-primary" onclick='javascript: return multiple_upload()' />
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <hr>
            <table class="table  table-bordered " id="test_table">
              <thead align="center">
                <tr class="tr_head tr_color" >
                  <th scope="col" width="5%"></th>
                  <th scope="col" width="20%">ชื่ออุปกรณ์</th>
                  <th scope="col" width="40%">IP</th>
                  <th scope="col" width="20%">เวลาล่าสุด</th>
                  <th scope="col" width="15%">สถานะ</th>
                </tr>
              </thead>
              <tbody id="body_t" align="center">

              </tbody>
            </table>
            <hr>

            <div class="row" id="frame">
            </div>

          </div>
          <br>
        </div>
      </div>
    </div>
  </div>
  <!-- ======== UserName Modal ========-->
  <div id="modal"></div>

  <!-- JS -->
  <script src="../JS/nav.js"></script>
  <script src="../JS/jquery.min.js"></script>
  <script src="../JS/bootstrap.min.js"></script>
  <script src="../JS/datatables.js"></script>
  <script src="../JS/moment.js"></script>


  <script type="text/javascript">

  var decode;

  $(document).ready(function() {
    // nav
    make_nav();
    admin("<?php echo $_SESSION['username'];?>");
    user();
    // nav
    insert_dy();
    // multiple_upload();
  });

  function make_nav(){

    nav("device","<?php echo $_SESSION['privilege'] ?>","<?php echo $_SESSION['name'] . " " . $_SESSION['surname'];?>");

    $('#dd').append(
      '<td> '+"<?php echo $_SESSION['username'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['name'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['surname'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['email'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['phone'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['create_date'];?>"+' </td>'
    );

  }

  function insert_dy(){
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "../pages/api/api_taa_dy.php",
      "method": "GET"
    }

    $.ajax(settings).done(function (response) {
      decode = JSON.parse(response);
      console.log(decode);
      for(var i=0; i<decode.Total; i++){
        $('#body_t').append(
          '<tr class="tr_body">'+
          // '<th scope="row" width="5%" bgcolor="#1D2731"><h4 style="color:#F4F6F7"><i class="far fa-id-card"></i></h4></th>'+
          '<th scope="row" width="5%" bgcolor="#1D2731"><a href="http://'+decode.List[i].ip+'"><h4 style="color:#F4F6F7"><i class="far fa-id-card"></i></h4></a></th>'+
          '<td width="20%" >  <a href="" data-toggle="modal" data-target="#'+decode.List[i].id+'">'+decode.List[i].model+'</a>'+
          // this is modal.
          '<form action="api/api_update_device.php" method="post">'+
            '<div class="modal fade" id="'+decode.List[i].id+'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog" role="document"><div class="modal-content"><div class="modal-header"><h5 class="modal-title" id="exampleModalLabel">แก้ไขชื่ออุปกรณ์ "'+decode.List[i].model+'"</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body">'+
            // body
            'ชื่ออุปกรณ์: <input name ="device_name_new">'+
            // end
            '</div><div class="modal-footer"><button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button><button type = "submit" name = "update" value = "'+decode.List[i].id+'" class="btn btn-primary">บันทึก</button></div></div></div></div>'+
          '</form>'+
          // '<td width="40%" >'+decode.List[i].ip+'</td>'+
          '<td width="40%" ><a href="http://'+decode.List[i].ip+'">'+decode.List[i].ip+'</a></td>'+
          '<td width="20%" >'+decode.List[i].lasttime+'</td>'+
          '<td width="15%" bgcolor="'+status_color(decode.List[i].lasttime)+'"><h5 style="color:#F4F6F7">'+status(decode.List[i].lasttime)+'</h5></td>'+
          '</tr>'
        );
      }
      $('#test_table').DataTable({});
    });
  }

  function status_color(lasttime){

    var a = moment(lasttime,'YYYY-MM-DD HH:mm:ss');
    var b = moment(moment().format('YYYY-MM-DD HH:mm:ss'),'YYYY-MM-DD HH:mm:ss');
    var sum = b.diff(a, "hours");

    if(sum < 2)
    return "#52BE80";
    if(sum >= 2)
    return "#DF0101";
    else
    return "#A4A4A4";

  }

  function status(lasttime){
    var a = moment(lasttime,'YYYY-MM-DD HH:mm:ss');
    var b = moment(moment().format('YYYY-MM-DD HH:mm:ss'),'YYYY-MM-DD HH:mm:ss');
    var sum = b.diff(a, "hours");

    if(sum < 2)
    return "ON";
    if(sum >= 2)
    return "OFF";
    else
    return "Unknown";
  }

  function multiple_upload(){

    $('#frame').empty();
    try {
      for(var i=0;i<decode.Total;i++){
        $('#frame').append(
            '<div class="col-lg-4 col-md-4 mb-2">'+
              '<p class="">'+decode.List[i].ip+'</p><hr>'+
              '<div class="embed-responsive embed-responsive-16by9">'+
                  '<iframe class="embed-responsive-item" name="frame_result'+i+'" width="100px" height="90px" style="height: 91%" allowfullscreen></iframe>'+
              '</div>'+
            '</div>'
          );
          console.log(decode.List[i].ip);
          document.forms['multiple_ip'].action='http://'+decode.List[i].ip+'/database/add_data_file.php';
          document.forms['multiple_ip'].target='frame_result'+i;
          document.forms['multiple_ip'].submit();
      }
    } catch (e) {
      console.log(e);
    }

    return true;

  }
</script>

</body>
</html>
