<?php
  session_start();
  $user = $_SESSION['username'];
  //$api_url=$_SESSION['url'];
  if( (!isset($_SESSION['valid'])) || ($_SESSION['valid']!=1)){
    header('Location: ../index.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>AGC-RFID</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../CSS/bootstrap.min.css">
  <link rel="stylesheet" href="../CSS/fonts.css">
  <!-- <link rel="stylesheet" href="../CSS/fontawesome-all.css"> -->
  <link rel="stylesheet" href="../CSS/fontawesome-all.min.css">
  <link rel="stylesheet" href="../CSS/sidenav.css">
  <link rel="stylesheet" href="../CSS/style.css">
  <link rel="stylesheet" href="../CSS/datatables.css">
  <link rel="stylesheet" href="../CSS/autocomplete.css">
  <style type="text/css">



  </style>
</head>
<body>
  <!-- As a heading -->
  <div id="nav"></div>
<br><br><br><br>
<div class="container-fluid">
  <div class="row">
    <div class="col-lg-2 col-md-2"></div>
    <div class="col-lg-10 col-md-10">
      <div class="panel panel-body card" id="main_panel">
        <br>
        <div class="container-fluid ">

          <h1 style="margin-left:20px"><i class="far fa-hand-point-up"></i> จัดการลายนิ้วมือ</h1>
          <hr>
          <br>
          <div class="col-md-12 col-lg-12">
            <form action="./api/data_finger.php" method="post">
              <div class="row">
                <div class="col-md-4 col-lg-4">
                  <table class="table table-striped table-hover table-bordered">
                    <thead>
                      <tr>
                        <th class="tr_head tr_color" style="vertical-align: baseline;">รหัสพนักงาน</th>
                        <td class="tr_body"><input type="text" class="form-control autocomplete" id="id" name="id" value=""></td>
                      </tr>
                    </thead>
                  </table>
                </div>
                <div class="col-md-4 col-lg-4">
                  <table class="table table-striped table-hover table-bordered">
                    <thead>
                    <tr>
                      <th class="tr_head tr_color" style="vertical-align: baseline;">รหัสลายนิ้วมือ</th>
                      <td class="tr_body"><input type="text" class="form-control" name="fingerid" placeholder="xxxx" value=""></td>
                    </tr>
                    </thead>
                  </table>
                </div>
                <div class="col-md-4 col-lg-4">
                  <button id="add" name="add" type="submit" class="btn btn-primary btn-lg btn-block">เพิ่ม</button>
                </div>
              </div>
            </form>
          </div>
          <br>
          <hr>
          <table class="table  table-bordered " id="show_table">
            <thead align="center">
              <tr class="tr_head tr_color" >
                <th scope="col">รหัสพนักงาน</th>
                <th scope="col">รหัสลายนิ้วมือ</th>
                <th scope="col">ชื่อต้น</th>
                <th scope="col">ชื่อ</th>
                <th scope="col">นามสกุล</th>

                <th scope="col"></th>
              </tr>
            </thead>
            <tbody id="body_t" align="center">

            </tbody>
            <br>
          </table>
        </div>
        <br>
        <div id="spinner">
            <center><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
            <span class="sr-only">Loading...</span></center>
        </div>
        <br>
      </div>
    </div>
  </div>
</div>



<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="modal" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content" id="modal_content">
      <div class="modal-header"></div>
      <div class="modal-body"></div>
      <div class="modal-footer"></div>
    </div>
  </div>
</div>

<!-- JS -->
<script src="../JS/nav.js"></script>
<script src="../JS/jquery.min.js"></script>
<script src="../JS/bootstrap.min.js"></script>
<script src="../JS/datatables.js"></script>

<script type="text/javascript">

  var decode;
  var select=[];

  $(document).ready(function() {
    check_input();
    var a = $('#ID').val();
    console.log(a);
    // nav
    make_nav();
    admin("<?php echo $_SESSION['username'];?>");
    // nav
    $('#spinner').show();

    show_emp();
    mainclick();
    autocomplete(document.getElementById("id"), select);

  });

  function make_nav(){
    nav("finger");
  }

  function show_emp(){
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "../pages/api/api_taa_empm.php",
      "method": "GET"
    }

    $.ajax(settings).done(function (response) {
      try {
        decode = JSON.parse(response);
        console.log(decode);
        for(var i=0; i<decode.Total; i++){

          $('#body_t').append(
            '<tr  class="tr_body" data-toggle="modal" data-target="#modal" value="'+decode.List[i].empn+'">'+
              '<td><B>'+decode.List[i].empn+'</B></td>'+
              '<td>'+decode.List[i].finger+'</td>'+
              '<td>'+decode.List[i].title+'</td>'+
              '<td>'+decode.List[i].fname+'</td>'+
              '<td>'+decode.List[i].surname+'</td>'+

              '<td align="center">'+

                  '<div class="btn-group btn-group-lg">'+
                    '<form action="./api/data_finger.php" method="post">'+
                      '<input type="hidden" name="del" value="'+decode.List[i].empn+'">'+
                      '<input type="hidden" name="finger" value="'+decode.List[i].finger+'">'+
                      '<button id="del'+i+'"  type="submit" class="btn btn-danger btn-md" style="display:none;"></button>'+
                    '</form>'+
                  '</div>'+
                  '<button id="del_confirm"  type="btn" class="btn btn-danger btn-md" onclick="confirm_del('+i+');">ลบ</button>'+
              '</td>'+
            '</tr>'
          );
          select.push(decode.List[i].empn);
        }
      } catch (e) {
        alert('ไม่พบข้อมูล');
        $('#spinner').hide();
      }


      $('#show_table').DataTable();
      $('#spinner').hide();
    });
  }

  function mainclick(){
    var id;
    $(".table").on('click','tr',function(e){
      console.log($(this).attr('value'));
      id = $(this).attr('value');
      reset_modal();
      create_modal(id);
    });

  }

  function reset_modal(){
      var modal = document.getElementById("modal_content");

      modal.innerHTML=
      '<form action="./api/data_finger.php" method="post">'+
          '<div class="modal-header" id="modal_header">'+
          '</div>'+
          '<div class="row modal-body" id="modal_body">'+
          '</div>'+
          '<div class="modal-footer" id="modal_footer">'+
          '</div>'+
      '</form>';
  }

  function create_modal(id){
    var fingerid
    for(var i=0; i<decode.Total; i++){
      if(decode.List[i].empn == id){

        if(decode.List[i].finger == null){
          fingerid = "กรุณาเพิ่มรหัสรายนิ้วมือก่อน"
        }else{
          fingerid = decode.List[i].finger;
        }

        $('#modal_header').append(
            '<h3 class="modal-title"><i class="fa fa-edit"></i> แก้ไขข้อมูล</h3>'+
            '<button type="button" class="close" data-dismiss="modal">&times;</button>'
        );
        $('#modal_body').append(
          '<div class="col-md-12 col-lg-12">'+
            '<table class="table table-striped table-hover table-bordered">'+
              '<thead align="center">'+
                '<tr>'+
                  '<th class="tr_head tr_color">รหัสพนักงาน</th>'+
                  '<td class="tr_body">'+
                    '<input type="hidden" class="form-control" name="id" value="'+decode.List[i].empn+'" >'+
                    '<input type="text" class="form-control"  value="'+decode.List[i].empn+'" disabled>'+
                  '</td>'+
                '</tr>'+
              '<tr>'+
                '<th class="tr_head tr_color">Card</th>'+
                '<td class="tr_body"><input type="text" class="form-control" name="card" value="'+decode.List[i].card+'" disabled></td>'+
              '</tr>'+
              '<tr>'+
                '<th class="tr_head tr_color">ชื่อต้น</th>'+
                '<td class="tr_body"><input type="text" class="form-control" name="title" value="'+decode.List[i].title+'" disabled></td>'+
              '</tr>'+
              '<tr>'+
                '<th class="tr_head tr_color">ชื่อ</th>'+
                '<td class="tr_body"><input type="text" class="form-control" name="name" value="'+decode.List[i].fname+'" disabled></td>'+
              '</tr>'+
              '<tr>'+
                '<th class="tr_head tr_color">นามสกุล</th>'+
                '<td class="tr_body"><input type="text" class="form-control" name="surname" value="'+decode.List[i].surname+'" disabled></td>'+
              '</tr>'+
              '<tr>'+
                '<th class="tr_head tr_color">รหัสลายนิ้วมือ</th>'+
                '<td class="tr_body"><input type="text" class="form-control" name="fingerid" value="'+fingerid+'"></td>'+
              '</tr>'+
              '</thead>'+
            '</table>'+
           '</div>'
        );
        $('#modal_footer').append(
            '<button name="edit" type="submit" class="btn btn-warning btn-lg">แก้ไข</button>'
        );
        break;
      }
    }

  }

  function autocomplete(inp, arr) {


    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function(e) {
        var a, b, i, val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) { return false;}
        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);
        /*for each item in the array...*/
        for (i = 0; i < arr.length; i++) {
          /*check if the item starts with the same letters as the text field value:*/
          if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
            /*create a DIV element for each matching element:*/
            b = document.createElement("DIV");
            /*make the matching letters bold:*/
            b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
            b.innerHTML += arr[i].substr(val.length);
            /*insert a input field that will hold the current array item's value:*/
            b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
            /*execute a function when someone clicks on the item value (DIV element):*/
                b.addEventListener("click", function(e) {
                /*insert the value for the autocomplete text field:*/
                inp.value = this.getElementsByTagName("input")[0].value;
                /*close the list of autocompleted values,
                (or any other open lists of autocompleted values:*/
                closeAllLists();
            });
            a.appendChild(b);
          }
        }
    });
    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function(e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
          /*If the arrow DOWN key is pressed,
          increase the currentFocus variable:*/
          currentFocus++;
          /*and and make the current item more visible:*/
          addActive(x);
        } else if (e.keyCode == 38) { //up
          /*If the arrow UP key is pressed,
          decrease the currentFocus variable:*/
          currentFocus--;
          /*and and make the current item more visible:*/
          addActive(x);
        } else if (e.keyCode == 13) {
          /*If the ENTER key is pressed, prevent the form from being submitted,*/
          e.preventDefault();
          if (currentFocus > -1) {
            /*and simulate a click on the "active" item:*/
            if (x) x[currentFocus].click();
          }
        }
    });
    function addActive(x) {
      /*a function to classify an item as "active":*/
      if (!x) return false;
      /*start by removing the "active" class on all items:*/
      removeActive(x);
      if (currentFocus >= x.length) currentFocus = 0;
      if (currentFocus < 0) currentFocus = (x.length - 1);
      /*add class "autocomplete-active":*/
      x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
      /*a function to remove the "active" class from all autocomplete items:*/
      for (var i = 0; i < x.length; i++) {
        x[i].classList.remove("autocomplete-active");
      }
    }
    function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
          if (elmnt != x[i] && elmnt != inp) {
          x[i].parentNode.removeChild(x[i]);
        }
      }
    }
    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
      closeAllLists(e.target);

    });


  }

  function check_input(){

    $("#add").click(function(){
    var id = $("#id").val().length;
    if(id == ''){
      alert('กรุณากรอกรหัสพนักงาน');
      return false;
    }
    }); //the end function

    $("#add").click(function(){
    var card = $("#card").val().length;
    if(card == ''){
      alert('กรุณากรอก CARD');
      return false;
    }
    }); //the end function


    $("#add").click(function(){
    var inname = $("#inname").val().length;
    if(inname == ''){
      alert('กรุณากรอกเลือกชื่อต้น');
      return false;
    }
    }); //the end function


    $("#add").click(function(){
    var name = $("#name").val().length;
    if(name == ''){
      alert('กรุณากรอกชื่อ ');
      return false;
    }
    }); //the end function


    $("#add").click(function(){
    var lastname = $("#lastname").val().length;
    if(lastname == ''){
      alert('กรุณากรอกนามสกุล');
      return false;
    }
    }); //the end function

  }

  function confirm_del(index){

    if (confirm("ยืนยันการลบ?")) {
        $('#del'+index).click();
    }

  }



</script>

</body>
</html>
