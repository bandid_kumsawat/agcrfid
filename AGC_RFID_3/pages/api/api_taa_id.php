<?php
  include 'connect.php';
  // On connect
  $arr = array();
  $arr_put = array();
  $i=0;
  $sql = "SELECT t1.empn,t1.CARD,t2.fname,t2.surname
          FROM taa_card t1
          INNER JOIN hr_dbo_view_card t2
          ON (t1.empn = t2.empn)";
  $result = $conn->query($sql);
  if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {

          $arr[$i] = array(
                             "empn"=>$row['empn'],
                             "card"=>$row['CARD'],
                             "fname"=>$row['fname'],
                             "surname"=>$row['surname']
                         );
           $i++;
      }
  } else {
      echo "0 results";
  }
  $conn->close();
  $arr_put = array("Total"=>$i,"List"=>$arr);
  echo (json_encode($arr_put)) ;
?>
