<?php
  include 'connect.php';
  // On connect
  $arr = array();
  $arr_put = array();
  $i=0;
  $sql = "SELECT *
          FROM taa_manager";
  $result = $conn->query($sql);
  if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {

          $arr[$i] = array(
                             "id"=>$row['ID'],
                             "ip"=>$row['IP'],
                             "type"=>$row['TYPE'],
                             "model"=>$row['MODEL'],
                             "hardware"=>$row['HARDWARE'],
                             "lasttime"=>$row['LASTTIME'],
                         );
           $i++;
      }
  } else {
      echo "0 results";
  }
  $conn->close();
  $arr_put = array("Total"=>$i,"List"=>$arr);
  echo (json_encode($arr_put)) ;
?>
