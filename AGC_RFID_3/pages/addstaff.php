<?php
  session_start();
  $user = $_SESSION['username'];
  //$api_url=$_SESSION['url'];
  if( (!isset($_SESSION['valid'])) || ($_SESSION['valid']!=1)){
    header('Location: ../index.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>AGC-RFID</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../CSS/bootstrap.min.css">
  <link rel="stylesheet" href="../CSS/fonts.css">
  <link rel="stylesheet" href="../CSS/fontawesome-all.css">
  <link rel="stylesheet" href="../CSS/fontawesome-all.min.css">
  <link rel="stylesheet" href="../CSS/sidenav.css">
  <link rel="stylesheet" href="../CSS/style.css">
  <link rel="stylesheet" href="../CSS/datatables.css">
  <link rel="stylesheet" type="text/css" href="../CSS/jquery.datetimepicker.css"/>
  <style type="text/css">


  </style>
</head>
<body>
  <!-- As a heading -->
  <div id="nav"></div>

<br><br><br><br>
<div class="container-fluid">
  <div class="row">
    <div class="col-lg-2 col-md-2"></div>
    <div class="col-lg-10 col-md-10">
    <div class="panel panel-body card" id="main_panel">
      <div class="container-fluid ">
        <br>
        <div class="row">
            <div class="col-lg-4 col-md-4">
              <br>
              <h1 style="margin-left:30px"><i class="far fa-address-book"></i> จัดการพนักงานพิเศษ</h1>
            </div>
            <div class="col-lg-1 col-md-1">
              <div class="vl"></div>
            </div>
              <div class="col-lg-6 col-md-6">

                <form action="./api/data_staff.php" method="post">
                    <div class="row">

                        <table class="table table-striped table-hover table-bordered">
                          <thead>
                            <tr>
                              <th class="tr_head tr_color" style="vertical-align: baseline;">ตำแหน่งพนักงานพิเศษ</th>
                              <td class="tr_body"><input type="text" class="form-control" name="type_s" placeholder="ใส่ตำแหน่งพนักงาน" value=""></td>
                              <td class="tr_body"><button id="add" name="add" type="submit" class="btn btn-primary btn-lg btn-block">เพิ่ม</button></td>
                            </tr>
                          </thead>
                        </table>
                    </div>
                  </div>
                </form>

        </div>
        <hr>
        <div class="container">
          <div class="row">
            <div class="col-lg-3 col-md-3"></div>
            <div class="col-lg-6 col-md-6">
              <table class="table" id="staff_table">
                <thead>
                  <tr class="tr_head tr_color">
                    <th scope="col">ตำแหน่งพนักงานพิเศษ</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody id="staff_body">
                  <!-- insert_data -->

                </tbody>
              </table>
            </div>
          </div>
        </div>
        <hr>
        <div class="container-fluid" id="head_t">
          <table class="table" id="main_table">
            <thead>
              <tr class="tr_head tr_color">
                <th scope="col">รหัสพนักงาน</th>
                <th scope="col">ชื่อต้น</th>
                <th scope="col">ชื่อ</th>
                <th scope="col">นามสกุล</th>
                <th scope="col">แผนก</th>
                <th scope="col">เบอร์โทร</th>
                <th scope="col">ตำแหน่ง</th>
              </tr>
            </thead>
            <tbody id="body_t">
              <!-- insert_data -->
            </tbody>
          </table>
        </div>
        <br>
        <div id="spinner">
            <center><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
            <span class="sr-only">Loading...</span></center>
        </div>
        <br>
      </div>
    </div>
  </div>
</div>
</div>
<!-- ======== UserName Modal ========-->
<div id="modal"></div>

<!-- JS -->
<script src="../JS/nav.js"></script>
<script src="../JS/jquery.min.js"></script>
<script src="../JS/bootstrap.min.js"></script>
<script src="../JS/jquery.datetimepicker.full.min.js"></script>
<script src="../JS/datatables.js"></script>
<script src="../JS/moment.js"></script>
<script src="../JS/moment.min.js"></script>

<script src="../JS/underscore-min.js"></script>
<script src="../JS/pdfmake.min.js"></script>
<script src="../JS/vfs_fonts.js"></script>
<script src="../JS/underscore-min.js"></script>

<script type="text/javascript">
  var data,st_date,end_date;
  var g_table,check_status;

  $(document).ready(function() {
    // nav
    make_nav();
    admin("<?php echo $_SESSION['username'];?>");
    user();
    // nav

    data_table();
  });

  function make_nav(){

    nav("addstaff","<?php echo $_SESSION['privilege'] ?>","<?php echo $_SESSION['name'] . " " . $_SESSION['surname'];?>");

    $('#dd').append(
      '<td> '+"<?php echo $_SESSION['username'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['name'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['surname'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['email'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['phone'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['create_date'];?>"+' </td>'
    );

  }




  function data_table(){

    var settings = {
      "async": true,
      "crossDomain": true,

      "url": "../pages/api/api_addstaff.php",
      "method": "GET"
    }

    $.ajax(settings).done(function (response) {
      try {
        data = JSON.parse(response)
        insert_data(data);
        staff_table(data);
      } catch (e) {
        console.log(e);
        alert('ไม่พบข้อมูล');
        $('#spinner').hide();
      }
    });
  }

  function staff_table(data_j){

      for(var i=0; i<data_j.List_s.length; i++){
        $('#staff_body').append(
            '<tr class="tr_body" style="text-align: center;">'+
              '<th scope="row" >'+data_j.List_s[i].type+'</th>'+

              '<td align="center">'+

                  '<div class="btn-group btn-group-lg">'+
                    '<form action="./api/data_staff.php" method="post">'+
                      '<input type="hidden" name="del" value="'+data_j.List_s[i].id+'">'+
                      '<button id="del'+i+'"  type="submit" class="btn btn-danger btn-md" style="display:none;"></button>'+
                    '</form>'+
                  '</div>'+
                  '<button id="del_confirm"  type="btn" class="btn btn-danger btn-md" onclick="confirm_del('+i+');">ลบ</button>'+
              '</td>'+
            '</tr>'
        );
      }
      $('#staff_table').DataTable({
        scrollY: 200
      });

  }

  function confirm_del(index){

    if (confirm("ยืนยันการลบ?")) {
        $('#del'+index).click();
    }

  }

  function insert_data(data_j){
    console.log(data_j);
    for(var i=0; i<data_j.Total; i++){
      $('#body_t').append(
          '<tr class="tr_body">'+
          '<th scope="row">'+data_j.List[i].empn+'</th>'+
          '<td >'+data_j.List[i].title+'</td>'+
          '<td>'+data_j.List[i].fname+'</td>'+
          '<td>'+data_j.List[i].surname+'</td>'+
          '<td>'+data_j.List[i].section2+'</td>'+
          '<td>'+data_j.List[i].tel+'</td>'+
          '<td>'+data_j.List[i].staff+'</td>'+
        '</tr>'
      );
    }



    $('#main_table').DataTable({
        dom: 'Bfrtip',
         buttons: [
            {
                extend: 'excelHtml5',
                title: 'staff_detail'
            }
        ]
    });
    $('#spinner').hide();
  }

</script>

</body>
</html>
