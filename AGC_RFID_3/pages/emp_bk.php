<?php
  session_start();
  $user = $_SESSION['username'];
  //$api_url=$_SESSION['url'];
  if( (!isset($_SESSION['valid'])) || ($_SESSION['valid']!=1)){
    header('Location: ../index.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>AGC-RFID</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../CSS/bootstrap.min.css">
  <link rel="stylesheet" href="../CSS/fonts.css">
  <link rel="stylesheet" href="../CSS/fontawesome-all.css">
  <link rel="stylesheet" href="../CSS/fontawesome-all.min.css">
  <link rel="stylesheet" href="../CSS/sidenav.css">
  <link rel="stylesheet" href="../CSS/style.css">
  <link rel="stylesheet" href="../CSS/datatables.css">
  <link rel="stylesheet" type="text/css" href="../CSS/jquery.datetimepicker.css"/>
  <style type="text/css">

  </style>
</head>
<body>
  <!-- As a heading -->
  <div id="nav"></div>

<br><br><br><br>
<div class="container-fluid">
  <div class="row">
    <div class="col-lg-2 col-md-2"></div>
    <div class="col-lg-10 col-md-10">
    <div class="panel panel-body card" id="main_panel">
      <div class="container-fluid ">
        <br>
        <div class="row">
            <div class="col-lg-4 col-md-4">
              <br>
              <h1><i class="far fa-address-card" style="margin-left:30px"></i> ตารางเวลาเข้าออก</h1>
              <!-- <div class="vll"></div> -->
            </div>

              <div class="col-lg-5 col-md-5">
                <div class="vl"></div>
                <div class="row">
                  <div class="col-lg-5 col-md-5">
                    <div class="form-group">
                      <label for="exampleFormControlFile1">วันที่เริ่มการค้นหา</label>
                      <input type="text" class="form-control" placeholder="วันที่ค้นหา" id="start_date" value="">
                    </div>
                  </form>
                  </div>
                  <div class="col-lg-5 col-md-5">
                    <div class="form-group">
                      <label for="exampleFormControlFile1">วันที่สิ้นสุดการค้นหา</label>
                      <input type="text" class="form-control" placeholder="วันที่ค้นหา" id="end_date" value="">
                    </div>
                  </form>
                  </div>
                  <div class="col-lg-2 col-md-2">
                    <br>
                    <button id="search" name="search" type="submit" class="btn btn-primary btn-lg">ดูข้อมูล</button>
                  </div>

                </div>
              </div>

              <div class="col-lg-3 col-md-3">
                <div class="vl"></div>
                <div class="row">
                  <div class="col-lg-3 col-md-3"></div>
                  <div class="col-lg-6 col-md-6">
                      <h3 id="real_date"></h3>
                    <hr>
                      <h3 id='txt' ><h3>
                  </div>
                  <div class="col-lg-3 col-md-3"></div>
                </div>
              </div>

        </div>
        <hr>
        <div class="container-fluid" id="head_t">
          <table class="table" id="main_table">
            <thead>
              <tr class="tr_head tr_color">
                <th scope="col">รหัสพนักงาน</th>
                <th scope="col">ชื่อต้น</th>
                <th scope="col">ชื่อ</th>
                <th scope="col">นามสกุล</th>
                <th scope="col">วันที่</th>
                <th scope="col">เวลาเข้า 1</th>
                <th scope="col">เวลาออก 1</th>
                <th scope="col">เวลาเข้า 2</th>
                <th scope="col">เวลาออก 2</th>
                <th scope="col">เวลาปฎิบัติงาน</th>
              </tr>
            </thead>
            <tbody id="body_t">
              <!-- insert_data -->
            </tbody>
          </table>
        </div>
        <br>
        <div id="spinner">
            <center><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
            <span class="sr-only">Loading...</span></center>
        </div>
        <br>
      </div>
    </div>
  </div>
</div>
</div>
<!-- ======== UserName Modal ========-->
<div id="modal"></div>

<!-- JS -->
<script src="../JS/nav.js"></script>
<script src="../JS/jquery.min.js"></script>
<script src="../JS/bootstrap.min.js"></script>
<script src="../JS/jquery.datetimepicker.full.min.js"></script>
<script src="../JS/datatables.js"></script>
<script src="../JS/moment.min.js"></script>

<script src="../JS/underscore-min.js"></script>
<script src="../JS/pdfmake.min.js"></script>
<script src="../JS/vfs_fonts.js"></script>
<script src="../JS/download.js"></script>

<script type="text/javascript">

  var data,st_date,end_date;
  var g_table;

  // var d_in1 = '172.16.73.150';
  // var d_in2 = '172.16.73.152';
  // var d_out1 = '172.16.73.151';
  // var d_out2 = '172.16.73.153';

  var d_in = ['172.16.73.150','172.16.73.152','172.16.73.154'];
  var d_out = ['172.16.73.151','172.16.73.153','172.16.73.155'];

  // test
    // var d_in = ['172.16.73.52','172.16.73.53','172.16.73.56'];
    // var d_out = ['172.16.73.54','172.16.73.55','172.16.73.57'];
  // test

  $(document).ready(function() {
    // nav
    make_nav();
    admin("<?php echo $_SESSION['username'];?>");
    user();
    // nav
    $('#real_date').text(moment().format('DD/MM/YYYY'));
    $('#txt').text(moment().format('HH : mm : ss'));
    time();

    $('#start_date').datetimepicker({
       timepicker:false,
       format:'Y-m-d'
    });
    $('#end_date').datetimepicker({
       timepicker:false,
       format:'Y-m-d'
    });
    $('#start_date').val(moment().format('YYYY-MM-DD'));
    $('#end_date').val(moment().format('YYYY-MM-DD'));
    // $('#start_date').val('2018-07-09');
    // $('#end_date').val('2018-07-09');

    try {
      search();
      $('#search').click();
    }
    catch(e) {
      // console.log(e);
    }

  });

  function make_nav(){

    nav("emp");

    $('#dd').append(
      '<td> '+"<?php echo $_SESSION['username'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['name'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['surname'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['email'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['phone'];?>"+' </td>'+
      '<td> '+"<?php echo $_SESSION['create_date'];?>"+' </td>'
    );

  }

  function search(){
    $('#search').click(function(){
      $('#head_t').empty();
      $('#head_t').append(
        '<table class="table" id="main_table">'+
          '<thead>'+
            '<tr class="tr_head tr_color">'+
              '<th scope="col">รหัสพนักงาน</th>'+
              '<th scope="col">ชื่อต้น</th>'+
              '<th scope="col">ชื่อ</th>'+
              '<th scope="col">นามสกุล</th>'+
              '<th scope="col">วันที่</th>'+
              '<th scope="col">เวลาเข้า 1</th>'+
              '<th scope="col">เวลาออก 1</th>'+
              '<th scope="col">เวลาเข้า 2</th>'+
              '<th scope="col">เวลาออก 2</th>'+
              '<th scope="col">เวลาปฎิบัติงาน</th>'+
            '</tr>'+
          '</thead>'+
          '<tbody id="body_t">'+
            '<!-- insert_data -->'+
          '</tbody>'+
        '</table>'

      );


      $('#body_t').empty();

      //---------------------------------------------------------------------------
      st_date = $('#start_date').val()+" 00:00:00";
      end_date = $('#end_date').val()+" 23:59:59";
      $('#spinner').show();
      data_table();
    });
  }

  function data_table(){

    var settings = {
      "async": true,
      "crossDomain": true,

      "url": "../pages/api/api_taa_emp_n.php?start="+st_date+"&end="+end_date,
      "method": "GET"
    }

    $.ajax(settings).done(function (response) {
      // data = JSON.parse(response)
      // insert_data_n(data);
      try {
        data = JSON.parse(response)
        insert_data_n(data);
      } catch (e) {
        // console.log(e);
        alert('ไม่พบข้อมูล');
        $('#spinner').hide();
      }
    });
  }

  function insert_data_n(data_j){

      console.log("!!! Raw !!!");
      console.log(data_j);
      var result_in_min,result_in_max,result_out_min,result_out_max;
      var temp_1 = _.sortBy(data_j.List,'date');
      console.log("!!! sorting !!!");
      // console.log(temp_1);


      var temp_2 = _.groupBy(temp_1,'empn');
      console.log("!!! group !!!");
      //console.log(temp_2);


      _.each(temp_2,function(val){
          
          console.log('Seperate by IP');
          var temp_3 = _.groupBy(val,'IP');
          console.log(temp_3);

          var data_in = [];
          for(var i=0; i<d_in.length; i++){
            data_in = _.union(data_in,temp_3[d_in[i]])
          }

          var data_out = [];
          for(var i=0; i<d_in.length; i++){
            data_out = _.union(data_out,temp_3[d_out[i]])
          }

          var data_in_sort = _.sortBy(data_in,"date");
          var data_out_sort = _.sortBy(data_out,"date");

          console.log(data_in_sort);
          console.log(data_out_sort);

          result_in_min = _.first(data_in_sort);
          result_in_max = _.last(data_in_sort);
          result_out_min = _.first(data_out_sort);
          result_out_max = _.last(data_out_sort);

          // -----------------------------------------------------------------
            // เอามาใส่ตรงนี้นะ
          // -----------------------------------------------------------------

          // console.log(result_in_min);
          // console.log(result_in_max);
          // console.log(result_out_min);
          // console.log(result_out_max);


          var emp_t = '-';
          var title_t = '-';
          var name_t = '-';
          var surname_t = '-';
          var date_t = '-';
          var hr_in_t = '-';
          var hr_out_t = '-';
          var hr_in_t1 = '-';
          var hr_out_t1 = '-';
          var status_t = '-';


          try {
            console.log(result_in_min === undefined);
            if(result_in_min !== undefined){
              emp_t   = result_in_min['empn'];
              title_t = result_in_min['title'];
              name_t  = result_in_min['fname'];
              surname_t = result_in_min['surname'];

              date_t  = filter_date(result_in_min['date'],0);
              hr_in_t = filter_date(result_in_min['date'],1);

              hr_out_t1 = filter_date(result_out_max['date'],1);

              if(result_out_min.date < result_in_max.date){
                hr_in_t1 = filter_date(result_in_max['date'],1);

                if(result_out_max.date > result_in_max.date){
                  hr_out_t = filter_date(result_out_min['date'],1);
                }
              }
            }else{
              emp_t   = result_out_min['empn'];
              title_t = result_out_min['title'];
              name_t  = result_out_min['fname'];
              surname_t = result_out_min['surname'];

              date_t  = filter_date(result_out_min['date'],0);
              // hr_out_t = filter_date(result_out_min['date'],1);
              hr_out_t1 = filter_date(result_out_max['date'],1);
            }
          } catch (e) {
            // console.log(e);
          }


          $('#body_t').append(
              '<tr class="tr_body">'+

              '<th scope="row">'+ emp_t +'</th>'+ //รหัสพนักงาน
              '<td >'+title_t+'</td>'+ //ชื่อต้น
              '<td>'+name_t+'</td>'+  //ชื่อ
              '<td>'+surname_t+'</td>'+ // นามสกุล
              '<td>'+date_t+'</td>'+
              '<td>'+hr_in_t+'</td>'+
              '<td>'+hr_out_t+'</td>'+
              '<td>'+hr_in_t1+'</td>'+
              '<td>'+hr_out_t1+'</td>'+
              '<td>'+ status_t +'</td>'+
            '</tr>'
          );
        
      });
      try {
        var table = $("#body_t");
        var a="-",b="-",sum,formatted,$tds;
        if(table != undefined){
          table.find('tr').each(function (i) {
                  $tds = $(this).find('td');
                  a =  moment($tds.eq(4).text(),"HH:mm:ss");
                  b =  moment($tds.eq(7).text(),"HH:mm:ss");

                  sum = b.diff(a,"second");
                  formatted = moment.utc(sum*1000).format('HH:mm' + " ชม.");
                  // console.log(sum);
                  if(isNaN(sum)){
                    if(a._i == "-"){
                      $tds.eq(8).text("-");
                    }
                    if(b._i == "-"){
                      $tds.eq(8).text("-");
                    }
                  }
                  else{
                    if(sum >= 0){
                      $tds.eq(8).text(formatted);
                    }
                    if(sum < 0){
                      $tds.eq(8).text(formatted);
                    }
                  }
          });
        }
      } catch (e) {}

      $('#main_table').DataTable({
            dom: 'Bfrtip',
            buttons: [
              {
                    text: 'TAF',
                    action: function ( e, dt, button, config ) {
                        var data = dt.buttons.exportData();
                        map_data(data);
                    }
                }
            ]
          });
      $('#spinner').hide();
      
  }

  function calculate_time(){

    var table = $("#body_t");

    table.find('tr').each(function (i) {
       var $tds = $(this).find('td'),
           productId = $tds.eq(3).text(),
           a =  moment($tds.eq(4).text(),"HH:mm:ss"),
           b =  moment($tds.eq(5).text(),"HH:mm:ss"),
           sum = b.diff(a,"second"),
           formatted = moment.utc(sum*1000).format('HH:mm' + " ชม.");
           console.log(sum);
           $tds.eq(6).text(formatted);
   });

  }

  function map_data(data){

    var ms = data.body;
    console.log(ms);

    var ret_day = _.groupBy(ms, function(data){
      return data[4];
    });

    console.log('Group By Date');
    console.log(ret_day);

    var arr = Object.keys(ret_day)
    var arr_date = _.sortBy(arr,function(input){return input;});

    // console.log(arr_date);
    // console.log(arr_date.length);

    // var auto_content = Array();
    var auto_content='';
    var p_break="";
    var text;

    for(var i=0; i< arr_date.length; i++){

      var  p_brake='';
      if(i!=0){
        p_brake = 'before'
      }

      // console.log(ret_day[arr_date[i]]);
      // console.log(arr_date[i].length);

      for(var j=0;j<ret_day[arr_date[i]].length;j++){

        if(ret_day[arr_date[i]][j][5] != "-"){
          text = ret_day[arr_date[i]][j][0]+'   I '+taf_date(ret_day[arr_date[i]][j][4])+' '+taf_time(ret_day[arr_date[i]][j][5])+' 01';
          auto_content = auto_content+text+'\r\n';
        }
        if(ret_day[arr_date[i]][j][6] != "-"){
          text = ret_day[arr_date[i]][j][0]+'   O '+taf_date(ret_day[arr_date[i]][j][4])+' '+taf_time(ret_day[arr_date[i]][j][6])+' 01';
          auto_content = auto_content+text+'\r\n';
        }
        if(ret_day[arr_date[i]][j][7] != "-"){
          text = ret_day[arr_date[i]][j][0]+'   I '+taf_date(ret_day[arr_date[i]][j][4])+' '+taf_time(ret_day[arr_date[i]][j][7])+' 02';
          auto_content = auto_content+text+'\r\n';
        }
        if(ret_day[arr_date[i]][j][8] != "-"){
          text = ret_day[arr_date[i]][j][0]+'   O '+taf_date(ret_day[arr_date[i]][j][4])+' '+taf_time(ret_day[arr_date[i]][j][8])+' 02';
          auto_content = auto_content+text+'\r\n';
        }

      }

      // console.log(text);

    }

      pdfMake.fonts = {
        THSarabunNew: {
          normal: 'THSarabunNew.ttf',
          bold: 'THSarabunNew-Bold.ttf',
          italics: 'THSarabunNew-Italic.ttf',
          bolditalics: 'THSarabunNew-BoldItalic.ttf'
        },
        Roboto: {
          normal: 'Roboto-Regular.ttf',
          bold: 'Roboto-Medium.ttf',
          italics: 'Roboto-Italic.ttf',
          bolditalics: 'Roboto-MediumItalic.ttf'
        }
      }

      var docDefinition = {

        content : auto_content,

        defaultStyle:{
          font:'THSarabunNew'
        },

      };

      // pdfMake.createPdf(docDefinition).download('test.txt');

      console.log(auto_content);

      download(auto_content, "Export.TAF", "text/plain");

  }

  // ----------------------------date TAF---------------

  function filter_date(date,select){
    if(select == 0){
      var date = date.split(" ")[0];
      // console.log(date);
      return date;
    }
    else if(select == 1){
      var time = date.split(" ")[1];
      // console.log(time);
      return time;
    }
    else{
      return "-";
    }

  }

  function taf_date(date){
    var data = date.split('-');
    return ''+data[0]+data[1]+data[2]
  }

  function taf_time(date){
    var data = date.split(':');
    return ''+data[0]+data[1]
  }

  // ----------------------------time-------------------
  function time(){
    setInterval(function(){
      $('#real_date').text(moment().format('DD/MM/YYYY'));
      $('#txt').text(moment().format('HH : mm : ss'));
    }, 1000);
  }



</script>

</body>
</html>
