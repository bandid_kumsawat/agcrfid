<?php
  include 'connect.php';
?>



<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Personnel</title>

  <link rel="stylesheet" href="../CSS/bootstrap.min.css">
  <link rel="stylesheet" href="../CSS/fonts.css">
  <link rel="stylesheet" href="../CSS/fontawesome-all.css">
  <link rel="stylesheet" href="../CSS/fontawesome-all.min.css">
  <link rel="stylesheet" href="../CSS/datatables.css">
  <link rel="stylesheet" type="text/css" href="../CSS/jquery.datetimepicker.css"/>


<style type="text/css">

  .hidden{
        display:none;
      }

</style>

</head>
<body>
<br>
<!-- ======== register========-->
<div class="container-fluid well">
  <div class="col-lg-12 col-md-12 ">

    <!-- add Personnel -->
    <div class="col-lg-12 col-md-12">

      <div class="panel panel-body card">
        <div class="container-fluid">

          <div class="col-md-12">
            <br>
            <h3><i class="fa fa-user-plus"></i> เพิ่มสมาชิก</h3>
            <div class="form-group">
              <table class="table table-bordered " >
                <thead>
                  <tr align="center" class="table-primary">
                    <th width="10%">UserName </th>
                    <th width="10%">Password </th>
                    <th width="10%">ชื่อ </th>
                    <th width="10%">นามสกุล </th>
                    <th width="10%">Email</th>
                    <th width="10%">เบอร์โทรศัพท์</th>
                    <th width="15%">CreateDateTime</th>
                    <th width="15%">privilege</th>
                    <th width="10%"></th>
                  </tr>
                </thead>
                <tbody id="register_add">
                  <tr>
                    <form action="sql.php" method="post">
                      <td> <input type="text" class="form-control" id="usrname" name="usrname" value=""> </td>
                      <td> <input type="text" class="form-control" id="password" name="password" value=""> </td>
                      <td> <input type="text" class="form-control" id="name" name="name" value=""> </td>
                      <td> <input type="text" class="form-control" id="surname" name="surname" value="">  </td>
                      <td> <input type="text" class="form-control" id="email" name="email" value="">  </td>
                      <td> <input type="text" class="form-control" id="phone" name="phone" value="">  </td>
                      <td> <input type='text' class="form-control" id="create_date" name="create_date" value=""> </td>
                      <td> 
                      <select type="text" class="form-control" id="privilege" name="privilege" value="">
                        <option value="1">HR</option>
                        <option value="2">Safety</option>
                        <option value="3">Other</option>
                      </select> 
                      </td>
                      <td align="center"><button id="add" name="add" type="submit" class="btn btn-primary btn-lg">เพิ่ม</button></td>
                    </form>
                  </tr>
                </tbody>
              </table>
              <div class='row'>
              <div class ='col-md-11 col-sm-11'></div>
              <div class ='col-md-1 col-sm-1'>
                <a href = "./../pages/emp.php"><button  type="button" class="btn btn-danger btn-lg">ออก</button></a>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
    <br>
    <!-- show data -->
    <div class="col-lg-12 col-md-12">
      <div class="panel panel-body card">
        <div class="container-fluid">

          <div class="col-md-12">
            <br>
            <h3><i class="fa fa-info-circle"></i> ข้อมูลสมาชิก</h3>
            <div class="form-group">
              <table class="table table-striped table-hover table-bordered" id="maintable">
                <thead >
                  <tr align="center" class="table-primary">
                    <th width="10%">UserName </th>
                    <th width="10%">Password </th>
                    <th width="10%">ชื่อ </th>
                    <th width="10%">นามสกุล </th>
                    <th width="10%">Email</th>
                    <th width="5%">เบอร์โทรศัพท์</th>
                    <th width="15%">CreateDateTime</th>
                    <th width="15%">privilege</th>
                    <th width="10%"></th>
                  </tr>
                </thead>
                <tbody id="register_detail">
                </tbody>
              </table>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="modal" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content" id="modal_content">
      <div class="modal-header"></div>
      <div class="modal-body"></div>
      <div class="modal-footer"></div>
    </div>
  </div>
</div>


<!-- JS include -->
<script src="../JS/jquery.min.js"></script>
<script src="../JS/bootstrap.min.js"></script>
<script src="../JS/jquery.datetimepicker.full.min.js"></script>
<script src="../JS/datatables.js"></script>

<script type="text/javascript">
var decode;
//------------------------------------------------- ready -----------------------------------------------------
$(document).ready(function(){
  $('#create_date').datetimepicker({
     timepicker:false,
     format:'d-m-Y h:i:s ',
  });

  $('#add').click(function(){
    console.log($('#usrname').val());
    if($('#usrname').val() == "" || $('#password').val() == ""){
      alert("ตรวจสอบ UserName หรือ Password");
    }
    if($('#name').val() == "" || $('#surname').val() == ""){
      alert("ตรวจสอบ ชื่อ หรือ นามสกุล");
    }
    if($('#email').val() == "" || $('#phone').val() == "" || $('#create_date').val() == ""){
      alert("ตรวจสอบ Email เบอร์โทรศัพท์ หรือ วันที่");
    }
  });

  create_table();
  mainclick();
});

function create_table(){
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "api_php.php",
    "method": "GET"
  }

  $.ajax(settings).done(function (response) {
    // console.log(response);
    decode = JSON.parse(response);
    console.log(decode);
    for(var i=0; i<decode.Total; i++){
      $('#register_detail').append(
          '<tr data-toggle="modal" data-target="#modal" value="'+decode.List[i].user_id+'">'+
                  '<td class="hidden">' + decode.List[i].user_id + '</td>'+
                  '<td>'+decode.List[i].username+'</td>'+
                  '<td>'+decode.List[i].password+'</td>'+
                  '<td>'+decode.List[i].name+'</td>'+
                  '<td>'+decode.List[i].surname+'</td>'+
                  '<td>'+decode.List[i].email+'</td>'+
                  '<td>'+decode.List[i].phone+'</td>'+
                  '<td>'+decode.List[i].create_date+'</td>'+
                  '<td>'+((decode.List[i].privilege == 1) ? "HR" : (decode.List[i].privilege == 2) ? "Sefety" : (decode.List[i].privilege == 3) ? "Other" : "admin")+'</td>'+
                  '<td align="center">'+

                      '<div class="btn-group btn-group-lg">'+

                        '<form action="sql.php" method="post">'+
                          '<input type="hidden" name="del" value="'+decode.List[i].user_id+'">'+
                          '<button id="del"  type="submit" class="btn btn-danger" >ลบ</button>'+
                        '</form>'+

                      '</div>'+

                  '</td>'+
          '</tr>'
      );
    }
  });
}

function mainclick(){
  var id;
  $(".table").on('click','tr',function(e){
    id = $(this).attr('value');
    reset_modal();
    create_modal(id);
  });

}

function reset_modal(){
    var modal = document.getElementById("modal_content");

    modal.innerHTML=
    '<form action="sql.php" method="post">'+
        '<div class="modal-header" id="modal_header">'+

        '</div>'+
        '<div class="row modal-body" id="modal_body">'+
        '</div>'+
        '<div class="modal-footer" id="modal_footer">'+
        '</div>'+
      '</form>';
}

function create_modal(id){

  // console.log(decode.Total);

  for(var i=0; i<decode.Total; i++){
    if(decode.List[i].user_id == id){
      $('#modal_header').append(
          '<h3 class="modal-title"><i class="fa fa-edit"></i> แก้ไขข้อมูลสมาชิก</h3>'+
          '<button type="button" class="close" data-dismiss="modal">&times;</button>'
      );
      $('#modal_body').append(
        '<div class="col-md-12 col-lg-12">'+
          '<table class="table table-striped table-hover table-bordered">'+
            '<tbody id="register_detail">'+
              '<tr align="center" class="table-warning">'+
                '<th width="6%">UserName </th>'+
                '<th width="6%">Password </th>'+
                '<th width="9%">ชื่อ </th>'+
                '<th width="7%">นามสกุล </th>'+
                '<th width="7%">Email</th>'+
                '<th width="7%">เบอร์โทรศัพท์</th>'+
                '<th width="3%">privilege</th>'+
              '</tr>'+

                '<td class="hidden"> <input type="text" class="form-control" id="usrname" name="user_id_e" value="'+decode.List[i].user_id+'"> </td>'+
                '<td> <input type="text" class="form-control" id="usrname" name="usrname_e" value="'+decode.List[i].username+'"> </td>'+
                '<td> <input type="text" class="form-control" id="password" name="password_e" value="'+decode.List[i].password+'"> </td>'+
                '<td> <input type="text" class="form-control" id="name" name="name_e" value="'+decode.List[i].name+'"> </td>'+
                '<td> <input type="text" class="form-control" id="surname" name="surname_e" value="'+decode.List[i].surname+'">  </td>'+
                '<td> <input type="text" class="form-control" id="email" name="email_e" value="'+decode.List[i].email+'">  </td>'+
                '<td> <input type="text" class="form-control" id="phone" name="phone_e" value="'+decode.List[i].phone+'">  </td>'+
                '<td>'+ 
                  '<select type="text" class="form-control" id="privilege" name="privilege" value="">'+
                    '<option value="1">HR</option>'+
                    '<option value="2">Safety</option>'+
                    '<option value="3">Other</option>'+
                  '</select>'+
                '</td>'+
            '</tbody>'+
          '</table>'+
         '</div>'
      );
      $('#modal_footer').append(
          '<button id="add" name="edit" type="submit" class="btn btn-warning btn-lg">แก้ไข</button>'
      );
      break;
    }
  }

}

// $('#maintable').DataTable();
</script>



</body>
</html>
