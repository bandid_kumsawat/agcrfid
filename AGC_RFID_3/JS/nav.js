function nav(page, privilege,name){
  console.log("name: "+name);
  var select = "";
  if(page == "emp"){
    // select = '<div id="mySidenav" class="sidenav">'+
    //             '<br><br><br><br>'+
    //               '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน<i class="fas fa-angle-double-right" style="margin-left:100px"></i></a>'+'<hr>'+
    //               '<a id="staff" href="staff.php"><i class="fas fa-diagnoses"></i> พนักงานพิเศษ</a>'+'<hr>'+
    //               '<a id="device" href="device.php"><i class="fas fa-tablet-alt"></i> อุปกรณ์</a>'+'<hr>'+
    //               '<a id="alert" href="alert.php"><i class="far fa-bell"></i> แจ้งเตือน</a>'+'<hr>'+
    //               '<a id="add" href="addemp.php"><i class="fas fa-user-plus"></i> จัดการพนักงาน</a>'+'<hr>'+
    //               // '<hr>'+
    //               //   '<a id="add" href="finger.php"><i class="far fa-hand-point-up"></i> จัดการลายนิ้วมือ</a>'+
    //               '<a id="staff_add" href="addstaff.php"><i class="far fa-address-book"></i> จัดการพนักงานพิเศษ</h1></a>'+'<hr>'+
    //               '<a id="raw" href="raw.php"><i class="fas fa-align-left"></i> ข้อมูลดิบ</a>'+'<hr>'+
    //           '</div>';
    select =  '<div id="mySidenav" class="sidenav"><br><br><br><br>' + 
                ((privilege == 1) ?  
                  '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน<i class="fas fa-angle-double-right" style="margin-left:100px"></i></a>'+'<hr>'+
                  '<a id="alert" href="alert.php"><i class="far fa-bell"></i> แจ้งเตือน</a>'+'<hr>'+
                  '<a id="add" href="addemp.php"><i class="fas fa-user-plus"></i> จัดการพนักงาน</a>'+'<hr>'+
                  '<a id="raw" href="raw.php"><i class="fas fa-align-left"></i> ข้อมูลดิบ</a>'+'<hr>'
                :
                  ((privilege == 2) ?
                    '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน<i class="fas fa-angle-double-right" style="margin-left:100px"></i></a>'+'<hr>'+
                    '<a id="staff" href="staff.php"><i class="fas fa-diagnoses"></i> พนักงานพิเศษ</a>'+'<hr>'+
                    '<a id="staff_add" href="addstaff.php"><i class="far fa-address-book"></i> จัดการพนักงานพิเศษ</h1></a>'+'<hr>'
                  :
                    ((privilege == 3) ?
                      '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน<i class="fas fa-angle-double-right" style="margin-left:100px"></i></a>'+'<hr>'+
                      '<a id="staff" href="staff.php"><i class="fas fa-diagnoses"></i> พนักงานพิเศษ</a>'+'<hr>'
                      :   
                        '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน<i class="fas fa-angle-double-right" style="margin-left:100px"></i></a>'+'<hr>'+
                        '<a id="staff" href="staff.php"><i class="fas fa-diagnoses"></i> พนักงานพิเศษ</a>'+'<hr>'+
                        '<a id="device" href="device.php"><i class="fas fa-tablet-alt"></i> อุปกรณ์</a>'+'<hr>'+
                        '<a id="alert" href="alert.php"><i class="far fa-bell"></i> แจ้งเตือน</a>'+'<hr>'+
                        '<a id="add" href="addemp.php"><i class="fas fa-user-plus"></i> จัดการพนักงาน</a>'+'<hr>'+
                        '<a id="staff_add" href="addstaff.php"><i class="far fa-address-book"></i> จัดการพนักงานพิเศษ</h1></a>'+'<hr>'+
                        '<a id="raw" href="raw.php"><i class="fas fa-align-left"></i> ข้อมูลดิบ</a>'+'<hr>'
                    )
                  )
                )
              +'</div>';
  }
  if(page == "staff"){
    // select = '<div id="mySidenav" class="sidenav">'+
    //             '<br><br><br><br>'+
    //               '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน</a>'+
    //             '<hr>'+
    //               '<a id="staff" href="emp.php"><i class="fas fa-diagnoses"></i> พนักงานพิเศษ<i class="fas fa-angle-double-right" style="margin-left:40px"></i></a>'+
    //             '<hr>'+
    //               '<a id="device" href="device.php"><i class="fas fa-tablet-alt"></i> อุปกรณ์</a>'+
    //             '<hr>'+
    //               '<a id="alert" href="alert.php"><i class="far fa-bell"></i> แจ้งเตือน</a>'+
    //             '<hr>'+
    //               '<a id="add" href="addemp.php"><i class="fas fa-user-plus"></i> จัดการพนักงาน</a>'+
    //             // '<hr>'+
    //             //   '<a id="add" href="finger.php"><i class="far fa-hand-point-up"></i> จัดการลายนิ้วมือ</a>'+
    //             '<hr>'+
    //               '<a id="staff_add" href="addstaff.php"><i class="far fa-address-book"></i> จัดการพนักงานพิเศษ</h1></a>'+
    //             '<hr>'+
    //               '<a id="raw" href="raw.php"><i class="fas fa-align-left"></i> ข้อมูลดิบ</a>'+
    //             '<hr>'+
    //           '</div>';
    select =  '<div id="mySidenav" class="sidenav"><br><br><br><br>' + 
                ((privilege == 1) ?  
                  '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน'+'<hr>'+
                  '<a id="alert" href="alert.php"><i class="far fa-bell"></i> แจ้งเตือน</a>'+'<hr>'+
                  '<a id="add" href="addemp.php"><i class="fas fa-user-plus"></i> จัดการพนักงาน</a>'+'<hr>'+
                  '<a id="raw" href="raw.php"><i class="fas fa-align-left"></i> ข้อมูลดิบ</a>'+'<hr>'
                :
                  ((privilege == 2) ?
                    '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน</a>'+'<hr>'+
                    '<a id="staff" href="staff.php"><i class="fas fa-diagnoses"></i> พนักงานพิเศษ<i class="fas fa-angle-double-right" style="margin-left:100px"></i></a></a>'+'<hr>'+
                    '<a id="staff_add" href="addstaff.php"><i class="far fa-address-book"></i> จัดการพนักงานพิเศษ</h1></a>'+'<hr>'
                  :
                    ((privilege == 3) ?
                      '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน</a>'+'<hr>'+
                      '<a id="staff" href="staff.php"><i class="fas fa-diagnoses"></i> พนักงานพิเศษ<i class="fas fa-angle-double-right" style="margin-left:100px"></i></a></a>'+'<hr>'
                      :   
                        '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน</a>'+'<hr>'+
                        '<a id="staff" href="staff.php"><i class="fas fa-diagnoses"></i> พนักงานพิเศษ<i class="fas fa-angle-double-right" style="margin-left:100px"></i></a></a>'+'<hr>'+
                        '<a id="device" href="device.php"><i class="fas fa-tablet-alt"></i> อุปกรณ์</a>'+'<hr>'+
                        '<a id="alert" href="alert.php"><i class="far fa-bell"></i> แจ้งเตือน</a>'+'<hr>'+
                        '<a id="add" href="addemp.php"><i class="fas fa-user-plus"></i> จัดการพนักงาน</a>'+'<hr>'+
                        '<a id="staff_add" href="addstaff.php"><i class="far fa-address-book"></i> จัดการพนักงานพิเศษ</h1></a>'+'<hr>'+
                        '<a id="raw" href="raw.php"><i class="fas fa-align-left"></i> ข้อมูลดิบ</a>'+'<hr>'
                    )
                  )
                )
              +'</div>';
  }
  if(page == "device"){
    // select = '<div id="mySidenav" class="sidenav">'+
    //             '<br><br><br><br>'+
    //               '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน</a>'+
    //             '<hr>'+
    //               '<a id="staff" href="staff.php"><i class="fas fa-diagnoses"></i> พนักงานพิเศษ</a>'+
    //             '<hr>'+
    //               '<a id="device" href="device.php"><i class="fas fa-tablet-alt"></i> อุปกรณ์<i class="fas fa-angle-double-right" style="margin-left:100px"></i></a>'+
    //             '<hr>'+
    //               '<a id="alert" href="alert.php"><i class="far fa-bell"></i> แจ้งเตือน</a>'+
    //             '<hr>'+
    //               '<a id="add" href="addemp.php"><i class="fas fa-user-plus"></i> จัดการพนักงาน</a>'+
    //             // '<hr>'+
    //             //   '<a id="add" href="finger.php"><i class="far fa-hand-point-up"></i> จัดการลายนิ้วมือ</a>'+
    //             '<hr>'+
    //               '<a id="staff_add" href="addstaff.php"><i class="far fa-address-book"></i> จัดการพนักงานพิเศษ</h1></a>'+
    //             '<hr>'+
    //               '<a id="raw" href="raw.php"><i class="fas fa-align-left"></i> ข้อมูลดิบ</a>'+
    //             '<hr>'+
    //           '</div>';
    select =  '<div id="mySidenav" class="sidenav"><br><br><br><br>' + 
                ((privilege == 1) ?  
                  '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน'+'<hr>'+
                  '<a id="alert" href="alert.php"><i class="far fa-bell"></i> แจ้งเตือน</a>'+'<hr>'+
                  '<a id="add" href="addemp.php"><i class="fas fa-user-plus"></i> จัดการพนักงาน</a>'+'<hr>'+
                  '<a id="raw" href="raw.php"><i class="fas fa-align-left"></i> ข้อมูลดิบ</a>'+'<hr>'
                :
                  ((privilege == 2) ?
                    '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน</a>'+'<hr>'+
                    '<a id="staff" href="staff.php"><i class="fas fa-diagnoses"></i> พนักงานพิเศษ</a></a>'+'<hr>'+
                    '<a id="staff_add" href="addstaff.php"><i class="far fa-address-book"></i> จัดการพนักงานพิเศษ</h1></a>'+'<hr>'
                  :
                    ((privilege == 3) ?
                      '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน</a>'+'<hr>'+
                      '<a id="staff" href="staff.php"><i class="fas fa-diagnoses"></i> พนักงานพิเศษ</a></a>'+'<hr>'
                      :   
                        '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน</a>'+'<hr>'+
                        '<a id="staff" href="staff.php"><i class="fas fa-diagnoses"></i> พนักงานพิเศษ</a></a>'+'<hr>'+
                        '<a id="device" href="device.php"><i class="fas fa-tablet-alt"></i> อุปกรณ์<i class="fas fa-angle-double-right" style="margin-left:100px"></i></a>'+'<hr>'+
                        '<a id="alert" href="alert.php"><i class="far fa-bell"></i> แจ้งเตือน</a>'+'<hr>'+
                        '<a id="add" href="addemp.php"><i class="fas fa-user-plus"></i> จัดการพนักงาน</a>'+'<hr>'+
                        '<a id="staff_add" href="addstaff.php"><i class="far fa-address-book"></i> จัดการพนักงานพิเศษ</h1></a>'+'<hr>'+
                        '<a id="raw" href="raw.php"><i class="fas fa-align-left"></i> ข้อมูลดิบ</a>'+'<hr>'
                    )
                  )
                )
              +'</div>';
  }
  if(page == "noti"){
    // select = '<div id="mySidenav" class="sidenav">'+
    //             '<br><br><br><br>'+
    //               '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน</a>'+
    //             '<hr>'+
    //               '<a id="staff" href="staff.php"><i class="fas fa-diagnoses"></i> พนักงานพิเศษ</a>'+
    //             '<hr>'+
    //               '<a id="device" href="device.php"><i class="fas fa-tablet-alt"></i> อุปกรณ์</a>'+
    //             '<hr>'+
    //               '<a id="alert" href="alert.php"><i class="far fa-bell"></i> แจ้งเตือน<i class="fas fa-angle-double-right" style="margin-left:90px"></i></a>'+
    //             '<hr>'+
    //               '<a id="add" href="addemp.php"><i class="fas fa-user-plus"></i> จัดการพนักงาน</a>'+
    //             // '<hr>'+
    //             //   '<a id="add" href="finger.php"><i class="far fa-hand-point-up"></i> จัดการลายนิ้วมือ</a>'+
    //             '<hr>'+
    //               '<a id="staff_add" href="addstaff.php"><i class="far fa-address-book"></i> จัดการพนักงานพิเศษ</h1></a>'+
    //             '<hr>'+
    //               '<a id="raw" href="raw.php"><i class="fas fa-align-left"></i> ข้อมูลดิบ</a>'+
    //             '<hr>'+
    //           '</div>';
    select =  '<div id="mySidenav" class="sidenav"><br><br><br><br>' + 
                ((privilege == 1) ?  
                  '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน'+'<hr>'+
                  '<a id="alert" href="alert.php"><i class="far fa-bell"></i> แจ้งเตือน<i class="fas fa-angle-double-right" style="margin-left:90px"></i></a>'+'<hr>'+
                  '<a id="add" href="addemp.php"><i class="fas fa-user-plus"></i> จัดการพนักงาน</a>'+'<hr>'+
                  '<a id="raw" href="raw.php"><i class="fas fa-align-left"></i> ข้อมูลดิบ</a>'+'<hr>'
                :
                  ((privilege == 2) ?
                    '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน</a>'+'<hr>'+
                    '<a id="staff" href="staff.php"><i class="fas fa-diagnoses"></i> พนักงานพิเศษ</a></a>'+'<hr>'+
                    '<a id="staff_add" href="addstaff.php"><i class="far fa-address-book"></i> จัดการพนักงานพิเศษ</h1></a>'+'<hr>'
                  :
                    ((privilege == 3) ?
                      '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน</a>'+'<hr>'+
                      '<a id="staff" href="staff.php"><i class="fas fa-diagnoses"></i> พนักงานพิเศษ</a></a>'+'<hr>'
                      :   
                        '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน</a>'+'<hr>'+
                        '<a id="staff" href="staff.php"><i class="fas fa-diagnoses"></i> พนักงานพิเศษ</a></a>'+'<hr>'+
                        '<a id="device" href="device.php"><i class="fas fa-tablet-alt"></i> อุปกรณ์</a>'+'<hr>'+
                        '<a id="alert" href="alert.php"><i class="far fa-bell"></i> แจ้งเตือน<i class="fas fa-angle-double-right" style="margin-left:90px"></i></a>'+'<hr>'+
                        '<a id="add" href="addemp.php"><i class="fas fa-user-plus"></i> จัดการพนักงาน</a>'+'<hr>'+
                        '<a id="staff_add" href="addstaff.php"><i class="far fa-address-book"></i> จัดการพนักงานพิเศษ</h1></a>'+'<hr>'+
                        '<a id="raw" href="raw.php"><i class="fas fa-align-left"></i> ข้อมูลดิบ</a>'+'<hr>'
                    )
                  )
                )
              +'</div>';
  }
  if(page == "add"){
    // select = '<div id="mySidenav" class="sidenav">'+
    //             '<br><br><br><br>'+
    //               '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน</a>'+
    //             '<hr>'+
    //               '<a id="staff" href="staff.php"><i class="fas fa-diagnoses"></i> พนักงานพิเศษ</a>'+
    //             '<hr>'+
    //               '<a id="device" href="device.php"><i class="fas fa-tablet-alt"></i> อุปกรณ์</a>'+
    //             '<hr>'+
    //               '<a id="alert" href="alert.php"><i class="far fa-bell"></i> แจ้งเตือน</a>'+
    //             '<hr>'+
    //               '<a id="add" href="addemp.php"><i class="fas fa-user-plus"></i> จัดการพนักงาน<i class="fas fa-angle-double-right" style="margin-left:30px"></i></a>'+
    //             // '<hr>'+
    //             //   '<a id="add" href="finger.php"><i class="far fa-hand-point-up"></i> จัดการลายนิ้วมือ</a>'+
    //             '<hr>'+
    //               '<a id="staff_add" href="addstaff.php"><i class="far fa-address-book"></i> จัดการพนักงานพิเศษ</h1></a>'+
    //             '<hr>'+
    //               '<a id="raw" href="raw.php"><i class="fas fa-align-left"></i> ข้อมูลดิบ</a>'+
    //             '<hr>'+
    //           '</div>';
    select =  '<div id="mySidenav" class="sidenav"><br><br><br><br>' + 
                ((privilege == 1) ?  
                  '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน'+'<hr>'+
                  '<a id="alert" href="alert.php"><i class="far fa-bell"></i> แจ้งเตือน</a>'+'<hr>'+
                  '<a id="add" href="addemp.php"><i class="fas fa-user-plus"></i>จัดการพนักงาน</a><i class="fas fa-angle-double-right" style="margin-left:30px"></i>'+'<hr>'+
                  '<a id="raw" href="raw.php"><i class="fas fa-align-left"></i> ข้อมูลดิบ</a>'+'<hr>'
                :
                  ((privilege == 2) ?
                    '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน</a>'+'<hr>'+
                    '<a id="staff" href="staff.php"><i class="fas fa-diagnoses"></i> พนักงานพิเศษ<i class="fas fa-angle-double-right" style="margin-left:100px"></a>'+'<hr>'+
                    '<a id="staff_add" href="addstaff.php"><i class="far fa-address-book"></i> จัดการพนักงานพิเศษ</h1></a>'+'<hr>'
                  :
                    ((privilege == 3) ?
                      '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน</a>'+'<hr>'+
                      '<a id="staff" href="staff.php"><i class="fas fa-diagnoses"></i> พนักงานพิเศษ</a></a>'+'<hr>'
                      :   
                        '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน</a>'+'<hr>'+
                        '<a id="staff" href="staff.php"><i class="fas fa-diagnoses"></i> พนักงานพิเศษ</a></a>'+'<hr>'+
                        '<a id="device" href="device.php"><i class="fas fa-tablet-alt"></i> อุปกรณ์</a>'+'<hr>'+
                        '<a id="alert" href="alert.php"><i class="far fa-bell"></i> แจ้งเตือน</a>'+'<hr>'+
                        '<a id="add" href="addemp.php"><i class="fas fa-user-plus"></i> จัดการพนักงาน<i class="fas fa-angle-double-right" style="margin-left:30px"></i></a>'+'<hr>'+
                        '<a id="staff_add" href="addstaff.php"><i class="far fa-address-book"></i> จัดการพนักงานพิเศษ</h1></a>'+'<hr>'+
                        '<a id="raw" href="raw.php"><i class="fas fa-align-left"></i> ข้อมูลดิบ</a>'+'<hr>'
                    )
                  )
                )
              +'</div>';
  }
  if(page == "finger"){
    // select = '<div id="mySidenav" class="sidenav">'+
    //             '<br><br><br><br>'+
    //               '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน</a>'+
    //             '<hr>'+
    //               '<a id="staff" href="staff.php"><i class="fas fa-diagnoses"></i> พนักงานพิเศษ</a>'+
    //             '<hr>'+
    //               '<a id="device" href="device.php"><i class="fas fa-tablet-alt"></i> อุปกรณ์</a>'+
    //             '<hr>'+
    //               '<a id="alert" href="alert.php"><i class="far fa-bell"></i> แจ้งเตือน</a>'+
    //             '<hr>'+
    //               '<a id="add" href="addemp.php"><i class="fas fa-user-plus"></i> จัดการพนักงาน</a>'+
    //             // '<hr>'+
    //             //   '<a id="add" href="finger.php"><i class="far fa-hand-point-up"></i> จัดการลายนิ้วมือ<i class="fas fa-angle-double-right" style="margin-left:25px"></i></a>'+
    //             '<hr>'+
    //               '<a id="staff_add" href="addstaff.php"><i class="far fa-address-book"></i> จัดการพนักงานพิเศษ</h1></a>'+
    //             '<hr>'+
    //               '<a id="raw" href="raw.php"><i class="fas fa-align-left"></i> ข้อมูลดิบ</a>'+
    //             '<hr>'+
    //           '</div>';
  }
  if(page == "addstaff"){
    // select = '<div id="mySidenav" class="sidenav">'+
    //             '<br><br><br><br>'+
    //               '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน</a>'+
    //             '<hr>'+
    //               '<a id="staff" href="staff.php"><i class="fas fa-diagnoses"></i> พนักงานพิเศษ</a>'+
    //             '<hr>'+
    //               '<a id="device" href="device.php"><i class="fas fa-tablet-alt"></i> อุปกรณ์</a>'+
    //             '<hr>'+
    //               '<a id="alert" href="alert.php"><i class="far fa-bell"></i> แจ้งเตือน</a>'+
    //             '<hr>'+
    //               '<a id="add" href="addemp.php"><i class="fas fa-user-plus"></i> จัดการพนักงาน</a>'+
    //             // '<hr>'+
    //             //   '<a id="add" href="finger.php"><i class="far fa-hand-point-up"></i> จัดการลายนิ้วมือ</a>'+
    //             '<hr>'+
    //               '<a id="staff_add" href="addstaff.php"><i class="far fa-address-book"></i> จัดการพนักงานพิเศษ</h1><i class="fas fa-angle-double-right" style="margin-left:25px"></i></a>'+
    //             '<hr>'+
    //               '<a id="raw" href="raw.php"><i class="fas fa-align-left"></i> ข้อมูลดิบ</a>'+
    //             '<hr>'+
    //           '</div>';
    select =  '<div id="mySidenav" class="sidenav"><br><br><br><br>' + 
                ((privilege == 1) ?  
                  '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน'+'<hr>'+
                  '<a id="alert" href="alert.php"><i class="far fa-bell"></i> แจ้งเตือน</a>'+'<hr>'+
                  '<a id="add" href="addemp.php"><i class="fas fa-user-plus"></i> จัดการพนักงาน</a>'+'<hr>'+
                  '<a id="raw" href="raw.php"><i class="fas fa-align-left"></i> ข้อมูลดิบ</a>'+'<hr>'
                :
                  ((privilege == 2) ?
                    '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน</a>'+'<hr>'+
                    '<a id="staff" href="staff.php"><i class="fas fa-diagnoses"></i> พนักงานพิเศษ</a></a>'+'<hr>'+
                    '<a id="staff_add" href="addstaff.php"><i class="far fa-address-book"></i> จัดการพนักงานพิเศษ<i class="fas fa-angle-double-right" style="margin-left:30px"></h1></a>'+'<hr>'
                  :
                    ((privilege == 3) ?
                      '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน</a>'+'<hr>'+
                      '<a id="staff" href="staff.php"><i class="fas fa-diagnoses"></i> พนักงานพิเศษ</a></a>'+'<hr>'
                      :   
                        '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน</a>'+'<hr>'+
                        '<a id="staff" href="staff.php"><i class="fas fa-diagnoses"></i> พนักงานพิเศษ</a></a>'+'<hr>'+
                        '<a id="device" href="device.php"><i class="fas fa-tablet-alt"></i> อุปกรณ์</a>'+'<hr>'+
                        '<a id="alert" href="alert.php"><i class="far fa-bell"></i> แจ้งเตือน</a>'+'<hr>'+
                        '<a id="add" href="addemp.php"><i class="fas fa-user-plus"></i> จัดการพนักงาน</a>'+'<hr>'+
                        '<a id="staff_add" href="addstaff.php"><i class="far fa-address-book"></i> จัดการพนักงานพิเศษ<i class="fas fa-angle-double-right" style="margin-left:30px"></h1></a>'+'<hr>'+
                        '<a id="raw" href="raw.php"><i class="fas fa-align-left"></i> ข้อมูลดิบ</a>'+'<hr>'
                    )
                  )
                )
              +'</div>';
  }
  if (page == "raw") {
    // select = '<div id="mySidenav" class="sidenav">' +
    //             '<br><br><br><br>' +
    //               '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน</a>' +
    //             '<hr>' +
    //               '<a id="staff" href="staff.php"><i class="fas fa-diagnoses"></i> พนักงานพิเศษ</a>' +
    //             '<hr>' +
    //               '<a id="device" href="device.php"><i class="fas fa-tablet-alt"></i> อุปกรณ์</a>' +
    //             '<hr>' +
    //               '<a id="alert" href="alert.php"><i class="far fa-bell"></i> แจ้งเตือน</a>' +
    //             '<hr>' +
    //               '<a id="add" href="addemp.php"><i class="fas fa-user-plus"></i> จัดการพนักงาน</a>' +
    //             // '<hr>' +
    //             //   '<a id="add" href="finger.php"><i class="far fa-hand-point-up"></i> จัดการลายนิ้วมือ</a>' +
    //             '<hr>' +
    //               '<a id="staff_add" href="addstaff.php"><i class="far fa-address-book"></i> จัดการพนักงานพิเศษ</h1></a>'+
    //             '<hr>'+
    //               '<a id="raw" href="raw.php"><i class="fas fa-align-left"></i> ข้อมูลดิบ<i class="fas fa-angle-double-right" style="margin-left:100px"></i></a>' +
    //             '<hr>' +
    //          '</div>';
    select =  '<div id="mySidenav" class="sidenav"><br><br><br><br>' + 
                ((privilege == 1) ?  
                  '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน'+'<hr>'+
                  '<a id="alert" href="alert.php"><i class="far fa-bell"></i> แจ้งเตือน</a>'+'<hr>'+
                  '<a id="add" href="addemp.php"><i class="fas fa-user-plus"></i> จัดการพนักงาน</a>'+'<hr>'+
                  '<a id="raw" href="raw.php"><i class="fas fa-align-left"></i> ข้อมูลดิบ<i class="fas fa-angle-double-right" style="margin-left:100px"></a>'+'<hr>'
                :
                  ((privilege == 2) ?
                    '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน</a>'+'<hr>'+
                    '<a id="staff" href="staff.php"><i class="fas fa-diagnoses"></i> พนักงานพิเศษ</a></a>'+'<hr>'+
                    '<a id="staff_add" href="addstaff.php"><i class="far fa-address-book"></i> จัดการพนักงานพิเศษ</h1></a>'+'<hr>'
                  :
                    ((privilege == 3) ?
                      '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน</a>'+'<hr>'+
                      '<a id="staff" href="staff.php"><i class="fas fa-diagnoses"></i> พนักงานพิเศษ</a></a>'+'<hr>'
                      :   
                        '<a id="emp" href="emp.php"><i class="far fa-address-card"></i> รายงาน</a>'+'<hr>'+
                        '<a id="staff" href="staff.php"><i class="fas fa-diagnoses"></i> พนักงานพิเศษ</a></a>'+'<hr>'+
                        '<a id="device" href="device.php"><i class="fas fa-tablet-alt"></i> อุปกรณ์</a>'+'<hr>'+
                        '<a id="alert" href="alert.php"><i class="far fa-bell"></i> แจ้งเตือน</a>'+'<hr>'+
                        '<a id="add" href="addemp.php"><i class="fas fa-user-plus"></i> จัดการพนักงาน</a>'+'<hr>'+
                        '<a id="staff_add" href="addstaff.php"><i class="far fa-address-book"></i> จัดการพนักงานพิเศษ</h1></a>'+'<hr>'+
                        '<a id="raw" href="raw.php"><i class="fas fa-align-left"></i> ข้อมูลดิบ<i class="fas fa-angle-double-right" style="margin-left:100px"></a>'+'<hr>'
                    )
                  )
                )
              +'</div>';
  }

  // ---------------------------------------------- NAV -------------------------------------------------
  $('#nav').append(
    '<nav class="navbar navbar-expand-lg navbar-light fixed-top" style="background-color: #a4c9f9;">'+
      '<div class="collapse navbar-collapse" id="navbarSupportedContent">'+
        '<ul class="navbar-nav mr-auto">'+
          '<li><img src="../img/logo_new.png" width="170px" height="60px" alt="" style="margin:0px 40px"></li>'+
        '</ul>'+

        '<div class="dropleft" >'+
          '<button class="btn btn-outline-dark dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
            '<i class="fas fa-user-circle"></i> '+name+
          '</button>'+
          '<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">'+
            '<a class="dropdown-item" href="../admin/logout.php">Log Out</a>'+
            '<hr>'+
            '<a class="dropdown-item" href="#" id="user_box">Detail</a>'+
            '<div id="admin_btn"></div>'+

        '</div>'+
      '</div>'+
      '</div>'+
    '</nav>'+select
  );
  if(page!="add")
    modal();

}
function modal(){
  // ---------------------------------------------- modal -------------------------------------------------
  $('#modal').append(
    '<div class="modal fade" id="user_modal">'+
      '<div class="modal-dialog modal-lg" role="document">'+
        '<div class="modal-content">'+

          '<div class="modal-header">'+
            '<div class="container-fluid">'+
              '<div class="col-md-6">'+
                '<h4 class="modal-title"><i class="far fa-address-book"></i> USER</h4>'+
              '</div>'+
          '</div>'+
        '</div>'+
        '<div class="modal-body">'+
          '<div class="container-fluid">'+

            '<div class="col-md-12">'+
              '<div class="form-group">'+
                '<table class="table table-striped table-hover table-bordered" >'+
                  '<tbody id="user_detail">'+
                    '<tr align="center" class="info">'+
                      '<th width="15%">UserName </th>'+
                      '<th width="15%">ชื่อ </th>'+
                      '<th width="15%">นามสกุล </th>'+
                      '<th width="15%">Email</th>'+
                      '<th width="15%">เบอร์โทรศัพท์</th>'+
                      '<th width="25%">CreateDateTime</th>'+
                    '</tr>'+
                    '<tr id="dd">'+

                    '</tr>'+
                  '</tbody>'+
                '</table>'+
              '</div>'+
            '</div>'+

          '</div>'+
        '</div>'+
      '</div>'+
    '</div>'+
  '</div>'
  );
}
function admin(admin){

  if( admin == "admin")
    $('#admin_btn').append('<hr><a class="dropdown-item" href="../admin/personnel.php">Admin</a>');

}
function user(){
  $('#user_box').click(function(){
    // $('#user_modal').modal('show');
  });
}
